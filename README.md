# ECCCORE_AVR - basic subsystem functions for developing AVR64x chips

The cod has been tested with AVR64DA48 and AVR64DD32.
While it is unlikely the entirety of this code will be applicable for projects,
it is a good reference for developing for the new Microchip AVR64x architecture.
Of special interest may be the clock and sound related functions.
These provide examples of using TCB0 and TCA0. The TCA0 code also demonstrates using alternate
pin assignments via PORTMUX
_(and highlight a limitation WITH CMP2 and FREQ mode which is not in the AVR64DD32 datasheet (DS40002315B)_.

This code was originally developed for the creation of ECC projects.
However, the code is useful in a range of projects which use the AVR764x 8 bit microchips.
The code is very compact and does not require Arduino or other external libraries.

To accommodate the desire to share as much as possible, the core code is public under the MIT license and
used as a GIT submodule within the specific ECC projects.

**Project Notes:**

The ECC projects uses an electronic schematic resembling a stripped down Arduino. There is no external oscillator,
and the power is a single 3V CR2032 or equivalent source. The CPU is run at 2 MHz and the UART operates at 9600,N,8,1.
A second UART may be run at the same or alternate BAUD rate _(but not above 9600 without changing the F_CPU clock speed)_.

A typical ECC has a series of LEDs, a series of touch sensors, and UART communications.
There is also support for a piezo buzzer.

The core supports the AVR POWER_DOWN sleep mode to conserve power. By default, the mode is activated after 60 seconds of inactivity.

Not all core features are tested by default from the sample code in `main.c`.

--------------------------------------------------------------------------
--------------------------------------------------------------------------


main() - a non-comprehensive example using portions of the ECC CORE functionality. (examine the code for more details)

Here are just a few useful notes:

## ECCCore Smoketest - basic subsystem test functions for an ECCCore based PCB

### OPERATING PRINCIPLE

The smoketest exercises each subsystem in series:

- This example requires user interaction to verify the LEDs and buzzer are working.
- The touch sensors are tested for any gross out-of-range values and then
- the sensor values are read and stored as the baseline values - eg calibration.
- The UART test assumes an external process sends a predefined string.
- The IR test assumes there the IR LED is optically looped back to the phototransistor.

### PORTAPROG INTEGRATION

The smoketest can work with any external test infrastructure capable of displaying the content
send from the smoketest over UART and capable of sending one or more predetermined text strings
which are validated by the smoketest code.

The code in this file was designed to work against a script running on a **PortaProg** portable programmer and test device.
The associated **PortaProg** test script is `smoketest.cmd` (provided with this repository).

The general overview of the `smoketest.cmd` script:

- sets the programmer to UPDI mode,
- writes the current firmware to the ECC device,
- powers the ECC device on (which begins the smoketest code),
- waits 5 seconds to receive a message containing the text 'BUZZER',
- sends the text 'UART' (which this smoketest code is looking for),
- waits for 6 seconds to receive a message containing the text 'SMOKETEST'
- which occurs at the end of the smoketest.

--------------------------------------------------------------------------
--------------------------------------------------------------------------


smoketest() - a comprehensive (?) test of the PCB to insure the build is successful


# config.h - define all configuration for the specific ECC project using the ATMega4809 microcontroller

All of the ECC subsystems are configured from this one file. It is important to match the definitions
below with the electrical schematic for all used IO Pins.

## Config Notes:

The subsystems use two AVR timers:
 - the clock uses TCB0
 - the buzzer uses TCA0

The original ECCCore was built for the ATMega328PB. The intent was to make it generic and have
all configuration driven the a file like this one. Well, that was a good idea and if this project
had hundreds of users, it would be be warranted. However, it is really only used by Bradán Lane STUDIO
so the decision was to fork the ECCCore _(twice now)_ with the switch to AVR64x chips. _Sorry (again)._

The ECCCore has been tested with AVR64DA48 and AVR64DD32.

The newer Atmel chip syntax for pins and port operations provides both bit numbers (bp) and bit masks (bm).
This implementation of ECCCore now uses bit masks to simplify code. *Ironically, Atmel is inconsistent
within the individual chip headers (eg `iom4809.h`) using HEX mask values in some cases and using bit-shift macros in other cases.*

Example: `PORTA.OUT |= (1 << PIN0_bp);` is the same as `PORTA.OUTSET = PIN0_bm;`

### User Defined Power Output Pins:
To conserve power during sleep, all attached peripherals must also be powered down.
A simple solution is to power them from GPIO pins. This assumes the peripheral power
requirements do not exceed the output of an GPIO pin.

If a peripheral is only needed for a short time, vs anytime the device is awake, then
that peripheral should have it's own pin.

**TIP:** There are many `#if 0` code fragments with the subsystems to help with debugging.
Most rely on UART working. As a general bit of advice, the _Sound_ or _LEDs_ subsystems are the
easiest to get running. This alow for some amount of status/feedback. UART is the most useful.

A Hardware debugger such as the Atmel ICE in conjunction with Microchip Studio _(formerly Atmel Studio)_
is the quickest way to get the basics working. In a pinch, an oscilloscope on a GPIO pin can help debug timers.
--------------------------------------------------------------------------

### CHIP Requirements:
The chip type drives some compiler preprocessing and is used to flag feature incompatibility.
Only one chip type may be defined. The various ISR vectors (interrupt handers) are an example of chip specific code.

#### PROGMEM_ENABLED
PROGMEM is the ability to store char constants in program space.

### Subsystem Enablement:
Most subsystems are optional and may be included/excluded as appropriate for the specific project.

#### POWER_ENABLED
The power subsystem provides both the sleep/wait functions and the sleep/wake button. It is only zero for debugging

#### EEPROM_ENABLED
EEPROM is the persistent storage of the microcontroller.

#### UART_ENABLED
The I/O UART is implemented on UART0.

#### IR_ENABLED
The I/O UART is implemented on UART0. The IR transceiver uses a second UART.

#### LEDS_ENABLED
LEDs are charlieplexed and use a timer. Even if LEDs are not enabled, the timer is still used to provide
a clock source for measuring events. Timer TCB0 is used.

#### TOUCH_ENABLED
TOUCH sensors measure the relative capacitance on an IO pin. All sensors are updated in a single synchronous function.

#### SOUND_ENABLED
The sound subsystem implements a one octave music scale consisting of 12 notes. The sound subsystem
assumes a piezoelectric buzzer on a specific pin to allow for using a hardware timer.

#### BUTTONS_ENABLED
The buttons subsystem provides convenience functions for checking the state of one or more buttons.

## Fixed Purpose Pins:
Very few pins of the ATMega4809 have explicit meaning but several have default capabilities
and are used within the ECCCore. It is important to uses these pins appropriately within
the project schematic. Most of the relevant restrictions are outlined in
the _I/O Multiplexing and Considerations_ table found in the microcontroller datasheet.

The USARTs on the AVR64x chips use PIN0 and PIN1 on PORTA, PORTC, PORTF, and PORTB corresponding to USART0 .. USART3.
Each USART also supports an alternate pin assignment using PIN4 and PIN5 on the same ports.
The code in `uart.h` assume the primary pin assignments are used.

### I/O Buffers:
**Assumptions:** The transmitting of data can be blocking if there is insufficient space to queue data into the TX buffer.
The receive buffer often needs to be larger to accommodate incoming data when other ECC activity delays pulling data off the RX buffer.
When also using the IR UART, it is not necessary they have the same buffer sizes if their purpose is significantly different.
When also using the RF, the TX buffer is limited by the size of the transceiver FRAME with is 127+1 bytes.

The IR circuit forms a hardware level UART. The photo-transistor requires power for its pull up.
Using a pin, rather than VCC, allows the circuit to be powered down to conserve battery
during deep sleep.

The wake from sleep uses a single GPIO pin

### Power Idle Timeouts:
The power-down sleep capability can act on idle detection. There are twe functions to quickly indicate interaction and reset the idle timeout.
The `NORMAL_SLEEP_DELAY` and `LONG_SLEEP_DELAY` definitions are used to control defaults for idle timeout.

#### SLEEP_MESSAGE_ENABLED
It can be handy to display a message when entering sleep mode and when exiting sleep mode.

IMPORTANT: don't forget to create defines for SLEEP_MESSAGE and WAKE_MESSAGE (below)

### SLEEP_MESSAGE Setup:
The sleep / wake / WARNING messages are not required. If any of the the `SLEEP_MESSAGE`, `WAKE_MESSAGE`, or  `WARNING_MESSAGE` definitions and not set, the corresponding code will be optimized away.

The sound subsystem uses the TCA0 hardware timer.
The AVR64DD32 has limited pins. To accommodate two UARTs, the BUZZER uses one of the alternative positions for TCA0.
If WO0 is not used, then an alternate timer mode is also needed.

### User Defined IO Pins:
Several general IO pins are needed within the core. It is important to match the definitions with the project schematic.

An ECC project may have buttons or interactions connected to pins.
Each button requires a port and a pin assignment. Currently all buttons are assumed pressed when LOW.

### LED IO Pins:
Discrete LEDs should be charlieplexed to reduce pin usage.
Regardless of charlieplexing, LEDs are be multiplexed to reduce power usage.

Charlieplexed LEDs require 2 pins for 2 LEDs; 3 pins for 6 LEDs; 4 pins for 12 LEDs; 5 pins for 20 LEDs; and 6 pins for 30 LEDs.

The code assumed all LED pins are within a single IO PORT.
RGB LEDs may have their `color` cathode pins in a separate PORT as the LED anode pins.

As the number of LEDs increases, the use of brightness _levels_ is discouraged.
When considering the number of LEDs, each discrete LED and each RGB LED counts as 1.

**WARNING: The LED implementation has changed and is not backward compatible !!!**

LEDs are now defined using a 16bit value which combines pin information, brightness levels, and optionally color (for RGB LEDs)

Each LED has the following representation: 0bRxHHHLLL 0bxEBBBCCC (where 'x' is a reserved/unused bit)
 - the first block is the 'pins' byte (which is constant) and is broken into three numbers:
      - R is a single bit indicating the LED is RGB vs a discrete color LED
      - HHH is a value 0..7 representing the anode (HIGH) pin
	  - LLL is a value 0..7 representing the cathode (LOW) pin
 - the second byte (runtime state) is broken into three numbers:
      - E is a single bit indicating the LED is enabled/disabled
	  - BBB is a value 0..7 representing brightness (limited to the value of LED_LEVELS)
	  - CCC is a value 0..7 representing color (ignored for 'D' discrete LEDs)


#### LED_ANIMATIONS
Animations are defined as a series of _frames_ where each frame is a complete copy of _leds_state.
The full animation is:
 - N the number of frames
 - T the duration of a full cycle of the animation
 - D an array of _leds_state (aka an array of arrays = LED_COUNT * N frames)

As you can see, LED animations are use significant memory. As an example, with 5 RGB LEDs and animation consisting of 9 frames
the data storage is 5 * 9  bytes = 45 bytes !YIKES!

The should be declared as `const` to store them in program memory.

### Touch Sensor IO Pins:
Touch sensors require a specific project schematic and PCB design.

The touch sensing is performed synchronously.

With only a single PORT, the maximum number of touch sensors is 8, but this only is possible when using a PORT with PIN0 thru PIN7 available.
Some PORTs have fewer available pins.

The code had been expanded - *and made a bit more complex* - to handle touch sensors across two PORTS. It would be smaller code and
slightly more efficient if all sensors were within a single AVR PORT. When `TOUCH_PORTS_2X` is ZERO, the code is optimized at compile time.

There are many different ways to define the Touch sensors. The minimum definitions and declarations include:
- `TOUCH_PORT_n?`
- `TOUCHn_PIN`
- `TOUCH_COUNT`
- `TOUCH_SPLIT` (when two ports are used)

The calibration of the touch sensors starts with a default: `TOUCH_DEFAULT_SINGLE_THRESHOLD`.
The touch sensors will attempt to self-calibrate.
If EEPROM is enabled and the `EEPROM_TOUCH_CALIBRATION` location offset is defined, then calibration will persist.

**NOTES:** It is rare that `TOUCH_MEASUREMENT_ITERATIONS` or `TOUCH_UPPER_LIMIT` need to be changed but there may
be cases where the touch range needs greater latitude.

The threshold and readings are based on the time it takes the code to test and loop.
For this reason, the values are affected by the MCU clock speed `F_CPU`. The initial values provided  here are based on tests
with a MCU running at 2MHz.

The default behavior is to treat touch sensors as switches. However, using `touchValue()` is is possible to react to the touch level.

### EEPROM Setup:
No specific configuration setup is *required* for the EEPROM. However, for `format` detection to work, a user defined bytecode
and location is required. These are optional definitions and the code will optimize away format detection if these are not defined.

If the `FORMAT_MARKER` definitions are present, the eeprom *format* related functions are useful for storing initial values in EEPROM.

*Note:* The ATMega4809 has only 256 bytes of EEPROM and the functions take use a relative address space starting at 0x0000


## leds.h - control a set of charlieplexed LEDs using the timer establish in `clock.h`

**Note:** Because this code integrates into the timer established in `clock.h`,
this module must be included before including `clock.h`.

While only a single LED is every ON at any instant, the use of the timer allows for the appearance
of any number of LEDs to be ON at any time. This conserves power and enables the use of charlieplexing
to minimize the number of pins needed for LEDs.

**Tip:** Charlieplexing require 2 pins for 2 LEDs; 3 pins for 6 LEDs; 4 pins for 12 LEDs; 5 pins for 20 LEDs; and 6 pins for 30 LEDs.

--------------------------------------------------------------------------

void ledSet(uint8_t num, uint8_t brightness, uint8_t color_index) - set LED color and brightness data

void ledsInit() - called to initialize the LEDs internal state structures and set all LEDs to OFF

bool ledsInited() - returns true is the LEDs module has been initialized

void ledOn(uint8_t num) - turn on an individual LED; it will remain on until turned off directly or when turning off all LEDs

void ledOff() - turn off an individual LED

void ledBlink(uint8_t num) - turn on and then off an LED one time very quickly - useful for testing

void ledsOn() - turn on all LEDs

void ledsOff() - turn off all LEDs

void ledsMultiOn() - turn on LEDs based on a bit mask

void ledsSequencer(uint8_t speed) - animate the LEDs once time in the sequence defined by the `_leds_order[]` array

void ledsAnimationStart(uint16_t duration, uint8_t count, LEDSTATE *sequence[])

Load and start an animation which will take `duration` to complete one full cycle.
The animation has `count` frames.
The `sequence` is an array of `count` frames.
Each `frame` is an array of LEDS_COUNT LEDSTATE items.
Thus, the total size of `sequence` data is `LEDSTATE[_count_][LEDS_COUNT]`.

void ledsAnimationStop() - end an active animation and return the LEDs to normal state operation using `ledOn()` etc.

void ledsAnimationSpeed(uint16_t duration) - change speed of active animation

uint_t ledsAnimationFrame() - get current frame (this is a moving target :-O )

void ledsDisable() - enable / disable the entire LED subsystem

void ledsEnable() - enable / disable the entire LED subsystem


## touch.h - digital touch sensor measurements

The touch sensing is based on measuring a capacitance change between a conductive
sensor created on the PCB and earth (self capacitance sensor).

The code measures the time for a pin to go from LOW to HIGH. When no capacitance is present,
the time is short. The time increases with the increase in capacitance.

The human body will act as a capacitor. So it is possible to detect with the sensor
is touched because the measure time increases.

Using a threshold, the capacitance touch sensor may be used as a momentary contact button.

A small sensor area will work with no isolation layer - aka without solder mask. However, there is the
risk of static discharge into the pin.

The sensor pin is momentarily pulled HIGH by the code.
Then allowed to dissipate through a large resistor (1M Ohm or larger).
The number of cycles of a tight loop is measured until the sensor pin is LOW again.
The more capacitance present, the longer it takes for the pin to be LOW.

The threshold value(s) are determined by the combination of the chosen resistor size,
the size of the touch pad on the PCB, and the presence or absence of solder mask.
It is possible that multiple sensors on a PCB will each have different thresholds.
The settings in `config.h` provides for this possibility.

**Note:** It is highly advisable to read all of the comments within the module to understand
the function of the circuit and its relation to the code.

**Wiring:**
 - connect 1M resistor between sensor pin and GND
 - connect sensor pin to touch pad on PCB

--------------------------------------------------------------------------

void touchClear() - clear all data associated with the touch sensors

uint16_t touchTouching() - compute the values for all touch sensors and return their state as a bit field (1 bit per sensor)

uint16_t touchTouched() - compute the values for all touch sensors and return their state as a bit field (1 bit per sensor) then clear the sensors

uint8_t touchValue(uint8_t num) - fetch the current numerical measurement for a single sensor (this is only valid after touchTouching())

void touchInit() - called to initialize the touch sensor state structures

bool touchInited() - returns true is the touch module has been initialized

bool touchCalibrate() - calibrate the touch sensors and save results to eeprom

uint8_t touchValue(uint8_t num) - fetch the current numerical measurement for a single sensor (this is only valid after touchTouching())


## sound.h - single musical note sound generator

This code is written for the ATMega4809. However, within the comments section you will find
the necessary timer values for the tinyAVR 1-series.

The frequency table is the musical scale starting at C3.

The megaAVR timer is 16bit to support multiple octaves of notes.

The macros `N`, `D`, and `T` are for creating music and sound files with a *packed byte* data format.
--------------------------------------------------------------------------

**Note:** We pack the musical notes into 8 bits: ddoonnnn = note duration, octave, note (C thru B)
to extend the capability for music, we use two special 'notes'. These special 'notes' are prefixes
and get the actual note and octave from the data which follows:
 - NOTE_DOTTED means it is 1.5x the duration; uses the next byte
 - NOTE_TRIPLET means the 3 notes fit in the given duration; uses the next 3 bytes

 There are macros to simplify created notes, dotted notes, and triplets: `N(n,o,d)`, `D(n,o,d)`, and `T(n1,o1,n2,o2,n3,o3,d)`.

 Note durations are limited to _WHOLE_, _HALF_, _QUARTER_, and _EIGHT_.

void soundInit() - perform initialization of the sound functions

void soundOff() - turn off sound and disable the timer

void soundFreq() - turn on sound of frequency by setting up the timer for hardware output on the `BUZZER_PIN`

NOTE: this function does not attempt to stop the current sound when setting a new one; it also does not attempt to start sound

void soundOn() - turn on sound of musical note by setting up the timer for hardware output on the `BUZZER_PIN`


## clock.h - a 1 millisecond reference timer value

The clock module uses TCB0 to establish a 2KHz timer and exposes it with 1 millisecond
accuracy. This is useful for performing timed operations, animations, detecting timeouts, etc.

The internal timer frequency could be changed to accommodate other uses.

In addition to being a simple instrument for timing things, the clock module also
handles the charlieplexed (or multiplexed) LEDs if/when they are enabled and initialized.

**Note 1:** 2Khz was chosen over 10Khz because the amount of time needed to perform the
interrupt task - both incrementing the clock and handling the LEDs.
The time to complete the interrupt affects the ultimate frequency. This may either
be tolerated or the compare/capture value adjusted to compensate.

Using an oscilloscope, a clock compensation value was determined.

It should be noted that this method is not perfect as the 'observer effect' may have effected the measurement
and thus the compensation value. Life sucks sometimes but you gotta do what you gotta do.

**Note 2:** When used in conjunction with LEDs, the led module must be included before the clock module.

--------------------------------------------------------------------------

void clockInit() - called to initialize the clock; this function should be called very early in the program execution

uint32_t clockMillis() - return the current counter as a 32bit unsigned integer. The counter starts at 0 when clockInit() is first called.


## eeprom.h - read/write interface to ATMega328PB eeprom memory

The module provides basic read/write of the microcontroller eeprom.

**Tip:** Writing a specific bytecode - _a marker byte_ - to a specific location of the EEPROM is an easy way to know if
it has been used before or if initial data should be written. If the bytecode is missing, then it can be
considered "unformatted" and any initial data should be written. If you change the data locations, then
changing the marker byte signals any stored data is likely unreliable.
See the `config.h` for additional details.

--------------------------------------------------------------------------

uint8_t eepromIsReady() - returns true if the eeprom is ready and false if it busy

void eepromWriteByte() - write a single byte to an eeprom memory location

void eepromReadByte() - read a single byte from eeprom memory relative to the eeprom base address

void eepromFormat() - set a marker byte to indicate the EEPROM has a known default state

bool eepromWasFormatted() - return true if the eeprom was formatted during the current boot/execution

bool eepromInited() - return true when the EEPROM is ready for use

void eepromInit() - initialization of the EEPROM functions and conditionally format

uint8_t* eepromSignature() - returns a pointer to a static buffer with our version the MCU unique ID

int eepromAddID() - store a chip ID but only if it has not already been stored


## power.h - sleep / wake functions

**Note:** Because this code uses the timer established in `clock.h`,
this module must be included after including `clock.h`.

The power module provides sleep and wake capability as well as idle timeout detection.
It preserves all registers and timers during the sleep cycle.
The wake is detected from INT0.

--------------------------------------------------------------------------

void powerInit() - perform necessary initialization of the power management subsystem

void powerButtonSleepEnable/Disable() - allow sleep mode to be activated using the wake button

void powerSleepUdate() - signal user action has occurred and reset the idle timeout

void powerSleepUdateLong() - signal user action has occurred and reset the idle timeout for a long period

void powerSleepNow() - begin sleep power shutdown process immediately; wake is triggered by INT0 being pulled low

void powerButtonPressed() - expose the wake/sleep button so it is available for other uses.
A common use will be to skip a long process (such as a game puzzle output).

void powerSleepConditionally() - begin sleep power shutdown process if the idle timer has expired.
This function should be called periodically from the main loop.


## random.h - pseudo random number generated

The random module provides a convenient source of pseudo random numbers. The generator is seeded from the noise of the internal ADC.

**Warning:** The seed is generated from the ADC using the temperature sensor.
If the ADC is high, then the value will always be 1023. Try altering the prescaler.

--------------------------------------------------------------------------

void randomInit() - initialize and seed the pseudo random number generator

uint8_t randomGetNum(max) - get an 8 bit  pseudo random number between zero and max

uint8_t randomGetByte() - get a pseudo random byte value between zero and 0xFF

uint16_t randomGetWord() - get a 16 bit pseudo random number

uint16_t randomGetSeed() - get the active pseudo random seed value

