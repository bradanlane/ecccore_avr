/* ************************************************************************************
* File:    smoketest.h
* Date:    2020.05.04
* Author:  Bradan Lane Studio
*
* This content may be redistributed and/or modified as outlined under the MIT License
*
* ************************************************************************************/

/* ---
## ECCCore Smoketest - basic subsystem test functions for an ECCCore based PCB

### OPERATING PRINCIPLE

The smoketest exercises each subsystem in series:

- This example requires user interaction to verify the LEDs and buzzer are working.
- The touch sensors are tested for any gross out-of-range values and then
- the sensor values are read and stored as the baseline values - eg calibration.
- The UART test assumes an external process sends a predefined string.
- The IR test assumes there the IR LED is optically looped back to the phototransistor.

### PORTAPROG INTEGRATION

The smoketest can work with any external test infrastructure capable of displaying the content
send from the smoketest over UART and capable of sending one or more predetermined text strings
which are validated by the smoketest code.

The code in this file was designed to work against a script running on a **PortaProg** portable programmer and test device.
The associated **PortaProg** test script is `smoketest.cmd` (provided with this repository).

The general overview of the `smoketest.cmd` script:

- sets the programmer to UPDI mode,
- writes the current firmware to the ECC device,
- powers the ECC device on (which begins the smoketest code),
- waits 5 seconds to receive a message containing the text 'BUZZER',
- sends the text 'UART' (which this smoketest code is looking for),
- waits for 6 seconds to receive a message containing the text 'SMOKETEST'
- which occurs at the end of the smoketest.

--------------------------------------------------------------------------
--------------------------------------------------------------------------

--- */

///////////////////////////////////////////////////////////////////////////////////////////////////


#define INTERACTIVE_TEST 0		// set to 1 if you want to do an interactive test vs automated / batch testing
#define LAST_MSG_DELAY 250		// wait time after last uart message to insure it is sent prior to any system shutdown task(s)

#define LOOP_DELAY		500		// timeout for any task(s) within the test loop
#define IR_MAX_ATTEMPTS 6		// number of loops (with timeout) to attempt the IR test

// we use these to beautify the smoketest messages; they are not strictly necessary
#define PORTAPROG_WIDTH			 40
#define PORTAPROG_MESSAGE_WIDTH	 36
#define PORTAPROG_RESPONSE_WIDTH 4


// macro for checking for touches
#define UPDATE_TOUCHES                \
	{                                 \
		touches = touchTouching();    \
		if (touches != old_touches) { \
			old_touches = touches;    \
			user_action = 1;          \
		} else {                      \
			user_action = 0;          \
		}                             \
	}

void _smoketest_error_indication() {
	for (uint8_t i = 0; i < 3; i++) {
		soundFreq(220);
		_delay_ms(1500);
		soundOff();
		_delay_ms(250);
	}
}

bool _smoketest_wait_for_continue() {
#if INTERACTIVE_TEST
	// with an interactive test, the test harness (aka a PortaProg) is sending messages to advance to the next test

	#define UART_MSG_TXT "NEXT" // must match the text sent from teh PortaProg script
	#define UART_MSG_LEN 5		// at least 1 byte longer than UART_MSG_TXT
	#define WAIT_LENGTH	 2000	// wait up to n seconds for the incoming mesage

	uint32_t duration = 0;
	duration = clockMillis() + WAIT_LENGTH;

	while (duration > clockMillis()) {
		// test UART (also used to mark valid completion of smoketest
		if (!uartReceiveBufferIsEmpty()) {
			bool dirty = false;
			uint8_t buff[UART_MSG_LEN + 1];
			uint8_t buff_len = 0;
			memset(buff, 0, UART_MSG_LEN + 1); // this guarantees the strings remains null terminated

			_delay_ms(100); // we delay a bit in the hope that we will not chunk up the incoming data
			//powerSleepUpdate();

			int c;
			while ((c = uartGetByte()) >= 0) {
				if (isprint(c)) { // dont send NULLs
					dirty = true;
					if (buff_len < UART_MSG_LEN)
						buff[buff_len++] = c;
				}
			}
			if (dirty) {
				if (!strcmp(buff, UART_MSG_TXT))
					return true;
				break;
			}
		}
	}
	printPrintf("script NEXT missing\n");	// only for debugging
	return false;
#else
	return true;
#endif
}

/* ---
smoketest() - a comprehensive (?) test of the PCB to insure the build is successful
--- */
int smoketest(void) {
	uint32_t duration = 0;
	uint16_t touches = 0, old_touches = 0;
	uint8_t user_action = 0;
	uint16_t count = 0;

	uint8_t just_woke_up = 1;
	uint8_t ir_attempts = 0;
	bool leds_buzzer_tested = false;
	bool touch_tested = false;
	bool seed_tested = false;
	bool ir_tested = false;
	bool uart_tested = false;

	bool reported = false;

	//_delay_ms(20000);		// give us a chance to look at the calibration

	while (1) {
		while (just_woke_up) {
			just_woke_up = false;
			leds_buzzer_tested = false;
			touch_tested = false;
			seed_tested = false;
			ir_tested = false;
			uart_tested = false;

			powerSleepUpdate();

			printPrintf("----------------------------------------\n");
			_delay_ms(500);

			// --------------------------------------------------------------
			// 0: manually verify LEDs and Buzzer
			// --------------------------------------------------------------
#if 1
			// should probably wrap these in #if with LEDS_ENABLE and SOUND_ENABLE
#if SOUND_ENABLE
			printPrintf("%d LEDS & TONE good? press WAKE button\n", LEDS_COUNT);
#else
			printPrintf("%d LEDS? press WAKE button\n", LEDS_COUNT);
#endif

			for (uint8_t i = 0; i < LEDS_COUNT; i++) {
				ledSet(i, 1, LED_LEVELS, COLOR_WHITE);
				//ledOn(i);
				soundFreq(440);
				_delay_ms(50);
				soundOff();
				ledOff(i);
			}
			soundFreq(880);
#ifdef LEDS_GAME_COUNT
			for (uint8_t i = 0; i < LEDS_GAME_COUNT; i++) {
				ledSet(i, 1, LED_LEVELS, COLOR_WHITE);
				//ledOn(i);
				_delay_ms(50);
			}
#else
			soundFreq(880);
			ledsOn();
#endif
			// wait up to 5 seconds for the tester to press the PWR button
			duration = clockMillis() + 5000;
			do {
				if (powerButtonPressed()) {
					leds_buzzer_tested = true;
					break;
				}
			} while (duration > clockMillis());
			soundOff();
			ledsOff();

			printPrintf("%-36s%s\n", "LEDS BUZZER TEST", (leds_buzzer_tested ? "PASS" : "FAIL"));
			_delay_ms(LAST_MSG_DELAY);

			powerSleepUpdate();
#endif

			// --------------------------------------------------------------
			// 0: verify no touch sensors are active
			// --------------------------------------------------------------
#if 1
			duration = clockMillis() + LOOP_DELAY;
			UPDATE_TOUCHES;
			if (touches) {
				printTouchData(touches);
				ledsMultiOn(touches);

				printPrintf("HANDSOFF ??\n");
				do {
					UPDATE_TOUCHES;
					printTouchData(touches);
					ledsMultiOn(touches);
					// if we take more than 5 seconds to clear the touch sensors, then it is likely a hardware issue
					if (duration < clockMillis()) {
						//uartPutStringNL("ERROR - TOUCH ACTIVE");
						_smoketest_error_indication();
						printPrintf("%-36s%s\n", "TOUCH IDLE", "FAIL");
						_delay_ms(LAST_MSG_DELAY);
					}
				} while (touches);
			}
			ledsOff();
			if (touches) break; // skip the rest of the test
#endif
			// --------------------------------------------------------------
			// 1: calibrate touch sensors
			// --------------------------------------------------------------
#if 1
			touch_tested = touchCalibrate();

			if (!touch_tested) {
				printPrintf("%-36s%s\n", "TOUCH CALIBRATION", "FAIL");
				_delay_ms(LAST_MSG_DELAY);
			} else {
				int great = true;
				// see if any touch values are high
				for (int i = 0; i < TOUCH_COUNT; i++) {
					if (touchCalibrationValue(i) > 16)
						great = false;
				}
				printPrintf("%-36s%s\n", "TOUCH CALIBRATION", (great ? "PASS" : "WARN"));
				// touch_tested is already set from result of touch Calibrate()
			}
			if (!_smoketest_wait_for_continue()) break;
#endif
			// --------------------------------------------------------------
			// 2: random seed generation
			// --------------------------------------------------------------
#if 1
			count = randomGetSeed();
			_delay_ms(100);
			randomInit(); // usually this is bad but we want to force a recalculation of the seed value
			// we should not get the same seed value twice
			count -= randomGetSeed();
			printPrintf("%-36s%s\n", "SEED TEST", (count ? "PASS" : "FAIL"));
			if (count) seed_tested = true;
			else
				_delay_ms(LAST_MSG_DELAY);
			if (!_smoketest_wait_for_continue()) break;
#endif
			// --------------------------------------------------------------
			// 3: prepare for IR test
			// --------------------------------------------------------------
#if 1
			irEnable();
			irFlushReceiveBuffer(); // this should be a no-op; drop anything remaining
			#define IR_MSG_TEXT "IR TEST"
			#define IR_MSG_LEN	7
			uint8_t buff[IR_MSG_LEN + 1];
			strcpy(buff, IR_MSG_TEXT);
			for (uint8_t i = 0; i < IR_MSG_LEN; i++) {
				irPutByte(buff[i]);
			}
			ir_attempts = 0;
			duration = clockMillis() + LOOP_DELAY; // take action every n seconds
#endif

			// get into a known state for the loop
			touches = old_touches = 0;
			user_action = 0;
			touchClear();
			UPDATE_TOUCHES;
		} // end of just_woke_up

		// --------------------------------------------------------------
		// 3: IR test
		// --------------------------------------------------------------
#if 1
		if ((duration < clockMillis()) || (!irReceiveBufferIsEmpty())) {
			uint8_t buff[IR_MSG_LEN + 1];
			uint8_t buff_len = 0;
			if (!irReceiveBufferIsEmpty()) {
				memset(buff, 0, IR_MSG_LEN + 1); // this guarantees there is always a null terminator
				_delay_ms(100);					 // we delay a bit in the hope that we will not chunk up the incoming data

				int c;
				while ((c = irGetByte()) >= 0) {
					if (buff_len < IR_MSG_LEN)
						buff[buff_len++] = c;
				}
				// not sure why I am seeing 1 byte that is a null
				if ((buff_len > 0) && (buff[0] == 0))
					buff_len = 0;

				irFlushReceiveBuffer(); // this should be a no-op; drop anything remaining

				// we will try IR_ATTEMPTS times to get a valid IR response
				if (ir_attempts < IR_MAX_ATTEMPTS) {
					ir_attempts++;

					if (strncmp(buff, IR_MSG_TEXT, 7) != 0) {
						if (ir_attempts == IR_MAX_ATTEMPTS) {
							// if not a match, then output what we got to help debug
							if (buff_len)
								printPrintf("%-16s [%02X%02X%02X%02X%02X%02X%02X] FAIL\n", "IR TEST", buff[0], buff[1], buff[2], buff[3], buff[4], buff[5], buff[6]);
							else
								printPrintf("%-36s%s\n", "IR TEST", "FAIL");
							ir_attempts++; // dont test any more
						} else {
							// reset the IR message so we can try again
							irFlushReceiveBuffer(); // this should be a no-op; drop anything remaining
							strcpy(buff, IR_MSG_TEXT);
							for (uint8_t i = 0; i < strlen(buff); i++) {
								irPutByte(buff[i]);
							}
							duration = clockMillis() + LOOP_DELAY; // delay out next attempt to read the ir
						}
					} else {
						printPrintf("%-36s%s\n", "IR TEST", "PASS");
						ir_attempts = IR_MAX_ATTEMPTS+1; // dont test any more
						ir_tested = true;
					}
				}
			}
			else if (ir_attempts < IR_MAX_ATTEMPTS) {
				ir_attempts++;
				duration = clockMillis() + LOOP_DELAY; // delay out next attempt to read the ir
				//printPrintf("IR ATTEMPT %d\n", ir_attempts);
				strcpy(buff, IR_MSG_TEXT);
				irFlushReceiveBuffer(); // this should be a no-op; drop anything remaining
				//irFlushTransmitBuffer(); // this should be a no-op; drop anything remaining
				for (uint8_t i = 0; i < strlen(buff); i++) {
					irPutByte(buff[i]);
				}
				powerSleepUpdate();

			}
			else {
				// if the timer runs out and we have not received the IR loopback data then we need to flag it as a failure
				if ((ir_attempts == IR_MAX_ATTEMPTS)) {
					printPrintf("%-36s%s\n", "IR TEST", "FAIL");
					ir_attempts++;
				}
			}
		}
#endif
		// --------------------------------------------------------------
		// 4: UART test
		// --------------------------------------------------------------
#if 1
		if (!uartReceiveBufferIsEmpty()) {
			bool dirty = false;
			#define UART_MSG_TEXT "UART"
			#define UART_MSG_LEN  5 // at least 1 byte longer than UART_MSG_TEXT
			uint8_t buff[UART_MSG_LEN + 1];
			uint8_t buff_len = 0;
			memset(buff, 0, UART_MSG_LEN + 1); // guarantees the buffer is always null terminated

			_delay_ms(100); // we delay a bit in the hope that we will not chunk up the incoming data

			int c;

			while ((c = uartGetByte()) >= 0) {
				if (isprint(c)) { // dont send NULLs
					dirty = true;

					// save incoming data to see if its a command
					if (buff_len < UART_MSG_LEN)
						buff[buff_len++] = c;
				} else if (c > 0) {
					dirty = true;
				}
			}
			if (dirty) {
				if (strcmp(buff, UART_MSG_TEXT) == 0)
					uart_tested = true;
				printPrintf("%-36s%s\n", "UART TEST", (uart_tested ? "PASS" :"FAIL"));
			}
		}
#endif

		// anything we may want to do while we wait for the smoketest to run out
#if 1
		UPDATE_TOUCHES;
		if (touches)
			powerSleepUpdate();
		if (user_action) {
			printTouchData(touches);
			// update the power timeout
			// light LEDs for each touch sensor pressed
			for (uint8_t i = 0; i < TOUCH_COUNT; i++) {
				if (touches & (1 << i)) {
					ledOn(i);
					soundMajorNote(i + 1, 2);
					_delay_ms(5);
				} else {
					ledOff(i);
				}
			}
			if (!touches)
				soundOff();
		}
#endif
		// --------------------------------------------------------------
		// ?: smoketest validation
		// --------------------------------------------------------------
#if 1
		if ((!reported) && touch_tested && seed_tested && ir_tested && uart_tested && leds_buzzer_tested) {
			reported = true;

			_delay_ms(LAST_MSG_DELAY);
			eepromWriteByte(EEPROM_VERIFIED, 1);

			printPrintf("----------------------------------------\n");
			printPrintf("%-36s%s\n", "SMOKETEST", "PASS");
			//printPrintf("----------------------------------------\n");
			eepromWriteByte(EEPROM_VERIFIED, 1);
			_delay_ms(LAST_MSG_DELAY);
			powerSleepNow();
		} else if ((duration + 100) < clockMillis()) {	// need a little extra delay so other tests finish
			if (!reported) {
				reported = true;
				// UART is the only interactive test that we could have missed
				if (!uart_tested) {
					printPrintf("%-36s%s\n", "UART TEST", "FAIL");
				}
				printPrintf("----------------------------------------\n");
				printPrintf("%-36s%s\n", "SMOKETEST", "FAIL");
			//printPrintf("----------------------------------------\n");
				_delay_ms(LAST_MSG_DELAY);
				//powerSleepNow();
				//just_woke_up = 1;
			}
		}
#endif
		// --------------------------------------------------------------
		// ?: let the user put the system to sleep
		// --------------------------------------------------------------
#if 1
		if (powerButtonPressed() && (!just_woke_up)) {
			irDisable();
			printPrintf("wake/sleep button pressed - force sleep\n");
			powerSleepNow();
			just_woke_up = 1;
		}
		// sleep if we have been idle too long
		if (powerSleepConditionally()) {
			just_woke_up = 1;
		}
#endif
	} // while(1) forever

	return (0); // should never get here, this is to prevent a compiler warning
}
