/* ************************************************************************************
* File:    uart.h
* Date:    2020.05.04
* Author:  Bradan Lane Studio
*
* This content may be redistributed and/or modified as outlined under the MIT License
*
* ************************************************************************************/

/* ---

## uart.h - UART driver with buffer support

**Important:** the CPU frequency (F_CPU) must be set correctly for the UART library
to calculate correct baud rates. Certain CPU frequencies will not produce exact baud
rates due to division round-off.

**Note:** To save power we do some funky stuff to only have the TX interupt
enabled when we have data to transmit.

--------------------------------------------------------------------------
--- */

//*****************************************************************************
// UART driver with buffer support.
// derived from Pascal Stang - Copyright (C) 2000-2002
//*****************************************************************************

#define IR_HIGH_POWER 0	// this powers the IR when enabled regardless of TXing or not ... seemed to be necessary ... at one point

// cBuffer structure
typedef struct struct_cBuffer {
	unsigned char *data;				  // the physical memory for the buffer
	unsigned short size;				  // the allocated size of the buffer
	unsigned short length;				  // the length of the data currently in the buffer
	unsigned short current;				  // the index into the buffer where the data starts
} cBuffer;


#define CRITICAL_SECTION_START	cli()
#define CRITICAL_SECTION_END	sei()
#define _cli()
#define _sei()
//#define _cli()	cli()
//#define _sei()	sei()

void bufferReset(cBuffer *buffer, uint8_t *data, uint16_t size) {
	// begin critical section
	CRITICAL_SECTION_START;
	buffer->data = data;
	buffer->size = size;
	// initialize index and length
	buffer->current = 0;
	buffer->length = 0;
	// end critical section
	CRITICAL_SECTION_END;
}

// access routines
int bufferPeek(cBuffer *buffer) {
	int rtn = -1;
	// begin critical section
	CRITICAL_SECTION_START;
	// check to see if there's data in the buffer
	if (buffer->length) {
		// get the first character from buffer
		rtn = buffer->data[buffer->current];
	}
	// end critical section
	CRITICAL_SECTION_END;
	// return
	return rtn;
}

// access routines
int bufferGet(cBuffer *buffer) {
	int rtn = -1;
	// begin critical section
	CRITICAL_SECTION_START;
	// check to see if there's data in the buffer
	if (buffer->length) {
		// get the first character from buffer
		rtn = buffer->data[buffer->current];
		// move index down and decrement length
		buffer->current++;
		if (buffer->current >= buffer->size)
			buffer->current -= buffer->size;
		buffer->length--;
	}
	// end critical section
	CRITICAL_SECTION_END;
	// return
	return rtn;
}

int bufferPut(cBuffer *buffer, uint8_t data) {
	int rtn = -1;

	// begin critical section
	CRITICAL_SECTION_START;
	// make sure the buffer has room
	if (buffer->length < buffer->size) {
		// save data byte at end of buffer
		buffer->data[(buffer->current + buffer->length) % buffer->size] = data;
		// increment the length
		buffer->length++;
		rtn = data;
	}
	// end critical section
	CRITICAL_SECTION_END;
	// return the original data character or failure
	return rtn;
}

int bufferPutWait(cBuffer *buffer, uint8_t data) {
	int rtn = -1;
	uint8_t attempts = 50;	// 50 * 5 ms = 250ms is the maximum we will wait to load the data
	while ((rtn = bufferPut(buffer, data)) < 0) {
		attempts--;
		if (!attempts)
			break;
		_delay_ms(5);
	}
	return rtn;
}

unsigned short bufferCapacity(cBuffer *buffer) {
	// begin critical section
	CRITICAL_SECTION_START;
	// check to see if the buffer has room
	// return true if there is room
	unsigned short bytesleft = (buffer->size - buffer->length);
	// end critical section
	CRITICAL_SECTION_END;
	return bytesleft;
}

unsigned short bufferEmpty(cBuffer *buffer) {
	// begin critical section
	CRITICAL_SECTION_START;
	// check to see if the buffer has room
	// return true if there is room
	unsigned short empty = true;
	if (buffer->length)
		empty = false;
	// end critical section
	CRITICAL_SECTION_END;
	return empty;
}

void bufferFlush(cBuffer *buffer) {
	// begin critical section
	CRITICAL_SECTION_START;
	// flush contents of the buffer
	buffer->length = 0;
	// end critical section
	CRITICAL_SECTION_END;
}

// uart structure
typedef struct struct_uart {
	uint8_t id;					// which usart (0 or 1); we track this to optimize some code
	cBuffer rxBuffer; 			// UART receive buffer
	cBuffer txBuffer; 			// UART transmit buffer
	unsigned short rxOverflow;	// receive overflow counter
	uint8_t txIdle;				// the TX buffer was emptied and the interrupt will need a jump start
} uartObj;

// receive and transmit buffers
static uint8_t uartRxData[UART_RX_BUFFER_SIZE];
static uint8_t uartTxData[UART_TX_BUFFER_SIZE];
static uartObj uart;
#if IR_ENABLED
// receive and transmit buffers
static uint8_t irRxData[IR_RX_BUFFER_SIZE];
static uint8_t irTxData[IR_TX_BUFFER_SIZE];
static uartObj ir;
#endif

#define UART_TIMEOUT 5000

// NOTE set photo-transistor power pin as output HIGH for whichever UART drives IR

#define UART_ENABLE_TX	{ _cli(); UART.CTRLA |= (USART_DREIE_bm);	/* UART_PORT.DIRSET = TX_PIN; UART_PORT.OUTSET = TX_PIN; */ _sei(); }
#define UART_DISABLE_TX	{ _cli(); UART.CTRLA &= ~(USART_DREIE_bm);	/* UART_PORT.DIRCLR = TX_PIN; UART_PORT.OUTCLR = TX_PIN; */ _sei(); }
#define UART_ENABLE_RX	{ _cli(); UART.CTRLA |= (USART_RXCIE_bm);  _sei(); }
#define UART_DISABLE_RX	{ _cli(); UART.CTRLA &= ~(USART_RXCIE_bm); _sei(); }

#if IR_ENABLED
#if IR_HIGH_POWER
#define IR_ENABLE_TX	{ _cli(); IR.CTRLA |= (USART_DREIE_bm);	/* IR_PORT.DIRSET = IR_TX_PIN; IR_PORT.OUTSET = IR_TX_PIN; */ _sei(); }
#define IR_DISABLE_TX	{ _cli(); IR.CTRLA &= ~(USART_DREIE_bm); /* IR_PORT.DIRCLR = IR_TX_PIN; IR_PORT.OUTCLR = IR_TX_PIN; */ _sei(); }
#else
#define IR_ENABLE_TX	{ _cli(); IR.CTRLA |= (USART_DREIE_bm);	IR_PORT.DIRSET = IR_TX_PIN; IR_PORT.OUTSET = IR_TX_PIN; _sei(); }
#define IR_DISABLE_TX	{ _cli(); IR.CTRLA &= ~(USART_DREIE_bm); IR_PORT.DIRSET = IR_TX_PIN; IR_PORT.OUTCLR = IR_TX_PIN; _sei(); }
#endif
#define IR_ENABLE_RX	{ _cli(); IR.CTRLA |= (USART_RXCIE_bm);  _sei(); }
#define IR_DISABLE_RX	{ _cli(); IR.CTRLA &= ~(USART_RXCIE_bm); _sei(); }
#endif

// flag variables
uint8_t _uart_inited;


// UART Transmit Complete Interrupt Handler - aka "data register empty" means we can send the next byte
ISR(UART_TX_INTERRUPT)
{
	if (uart.txBuffer.length) {
		//UART_PORT.CTRLA &= ~USART_DREIE_bm;	// Turn off this interrupt

		// send byte from top of buffer
		//UART_PORT.STATUS = USART_TXCIF_bm;
		UART.TXDATAL = (unsigned char)bufferGet(&(uart.txBuffer));
		//UART_PORT.CTRLA |= USART_DREIE_bm;	// Turn on this interrupt
	}
	else {
		uart.txIdle = true;
		// buffer is empty, turn off interrupt
		UART_DISABLE_TX;
	}
}

// UART Receive Complete Interrupt Handler - aka there is data so grab it
ISR(UART_RX_INTERRUPT) {
	unsigned char c;

	// get received char
	c = UART.RXDATAL;	// this also clears the interrupt flag for us

	// put received char in buffer; // check if there's space; // mark overflow if we could not store the new data
	if (bufferPut(&(uart.rxBuffer), c) < 0)
		(uart.rxOverflow)++; // no space in buffer; count overflow
	else
		uart.rxOverflow = 0;
}


#if IR_ENABLED

// UART Transmit Complete Interrupt Handler - aka "data register empty" means we can send the next byte
ISR(IR_TX_INTERRUPT)
{
	if (ir.txBuffer.length) {
		//UART_PORT.CTRLA &= ~USART_DREIE_bm;	// Turn off this interrupt

		// send byte from top of buffer
		//UART_PORT.STATUS = USART_TXCIF_bm;
		IR.TXDATAL = (unsigned char)bufferGet(&(ir.txBuffer));
		//UART_PORT.CTRLA |= USART_DREIE_bm;	// Turn on this interrupt
	}
	else {
		ir.txIdle = true;
		// buffer is empty, turn off interrupt
		IR_DISABLE_TX;
	}
}

// UART Receive Complete Interrupt Handler - aka there is data so grab it
ISR(IR_RX_INTERRUPT) {
	unsigned char c;

	// get received char
	c = IR.RXDATAL;	// this also clears the interrupt flag for us

	// put received char in buffer; // check if there's space; // mark overflow if we could not store the new data
	if (bufferPut(&(ir.rxBuffer), c) < 0)
		(ir.rxOverflow)++; // no space in buffer; count overflow
	else
		ir.rxOverflow = 0;
}

#endif



/* ---
void uartInit() - called to initialize the UART buffers and interrupts
--- */
void uartInit() {
	_uart_inited = false;

	uint32_t baudrate;

	baudrate = UART_BAUD;
	uart.id = 0;
	// initialize the buffers
	bufferReset(&(uart.rxBuffer), uartRxData, UART_RX_BUFFER_SIZE); // initialize the UART receive buffer
	bufferReset(&(uart.txBuffer), uartTxData, UART_TX_BUFFER_SIZE); // initialize the UART transmit buffer

	cli(); // prevent interrupts

	//UART.DBGCTRL = USART_DBGRUN_bm;	// enable run in debug

	// set  baud rate
	UART.BAUD = (uint16_t)BAUD_RATE_FORMULA(baudrate);

	UART_PORT.DIRSET = TX_PIN;				// TX pin is output
	UART_PORT.OUTSET = TX_PIN;				// TX is HIGH
	UART_PORT.DIRCLR = RX_PIN;				// RX pin is input
	UART_PORT.PIN0CTRL |= PORT_PULLUPEN_bm;	// RX has internal pull-up

	//#define SERIAL_8N1 (USART_CMODE_ASYNCHRONOUS_gc | USART_CHSIZE_8BIT_gc | USART_PMODE_DISABLED_gc | USART_SBMODE_1BIT_gc)
	//UART_PORT.CTRLC = SERIAL_8N1;

	UART.CTRLB |= (USART_TXEN_bm) | (USART_RXEN_bm);	// enable TX and RX
	UART_ENABLE_RX;											// enable RX interrupt

#if defined (CHIP_AVR64DD32)
	//UART.CTRLC = USART_CMODE_ASYNCHRONOUS_gc | USART_PMODE_DISABLED_gc | USART_SBMODE_1BIT_gc | USART_CHSIZE_8BIT_gc;
#endif
	sei(); // enable interrupts

	// establish our initial state
	uart.rxOverflow = 0;
	uart.txIdle = true;

#if IR_ENABLED

	baudrate = IR_BAUD;
	ir.id = 1;
	// initialize the buffers
	bufferReset(&(ir.rxBuffer), irRxData, IR_RX_BUFFER_SIZE); // initialize the UART receive buffer
	bufferReset(&(ir.txBuffer), irTxData, IR_TX_BUFFER_SIZE); // initialize the UART transmit buffer

	cli(); // prevent interrupts

	// set  baud rate
	IR.BAUD = (uint16_t)BAUD_RATE_FORMULA(baudrate);

#if IR_HIGH_POWER
	IR_PORT.DIRSET = IR_TX_PIN;				// TX pin is output
	IR_PORT.OUTSET = IR_TX_PIN;				// TX is HIGH to start
#endif
	IR_PORT.DIRCLR = IR_RX_PIN;				// RX pin is input
	IR_PORT.PIN0CTRL |= PORT_PULLUPEN_bm;	// RX has internal pull-up

	//#define SERIAL_8N1 (USART_CMODE_ASYNCHRONOUS_gc | USART_CHSIZE_8BIT_gc | USART_PMODE_DISABLED_gc | USART_SBMODE_1BIT_gc)
	//UART_PORT.CTRLC = SERIAL_8N1;

	IR.CTRLB |= (USART_TXEN_bm) | (USART_RXEN_bm);		// enable TX and RX
	IR_ENABLE_RX;										// enable RX interrupt

	sei(); // enable interrupts

	// establish our initial state
	ir.rxOverflow = 0;
	ir.txIdle = true;

	// setup IR port power; but initial state is OFF
	IR_PWR_PORT.DIRSET = IR_PWR_PIN;
	IR_PWR_PORT.OUTCLR = IR_PWR_PIN;

#endif

	_uart_inited = true;
}

/* ---
bool uartInited() - returns true is the UART module has been initialized
--- */
bool uartIsInited() {
	return _uart_inited;
}





/* ---
#### UART Helper Functions
--- */

/* ---
void uartFlushReceiveBuffer() - flush any pending data in the receive buffer (useful if you are waiting on a specific message and have detected it is corrupted)
--- */
void _uartFlushReceiveBuffer(uartObj* p) {
	// flush all data from receive buffer
	bufferFlush(&(p->rxBuffer));
	p->rxOverflow = 0;
}

/* ---
void uartFlushTransmitBuffer() - flush any pending data in the receive buffer (useful if you are waiting on a specific message and have detected it is corrupted)
--- */
void _uartFlushTransmitBuffer(uartObj* p) {
	// flush all data from receive buffer
	bufferFlush(&(p->txBuffer));
	p->txIdle = true;
	if (p->id == 0) {
		UART_DISABLE_TX;
	}
#if IR_ENABLED
	else {
		IR_DISABLE_TX;
	}
#endif
}

/* ---
bool uartReceiveBufferOverflow() - returns true if the receive buffer has filled up (a good indication that subsequent data is unreliable)
--- */
uint8_t _uartReceiveBufferOverflow(uartObj* p) {
	return p->rxOverflow; // no space in buffer; count overflow
}

/* ---
bool uartReceiveBufferIsEmpty() - returns true if there is no data available
--- */
uint8_t _uartReceiveBufferIsEmpty(uartObj* p) {
	if (!_uart_inited)
		return true;
	if (p->rxBuffer.length == 0)
		return true;
	else
		return false;
}

/* ---
bool uartTransmitBufferIsEmpty() - returns true if there is no data left to transmit
--- */
uint8_t _uartTransmitBufferIsEmpty(uartObj* p) {
	if (!_uart_inited)
		return true;
	if (p->txBuffer.length == 0)
		return true;
	return false;
}


/* ---
#### UART Read Functions
--- */


/* ---
int uartGetByte() - gets a single byte from the receive buffer (getchar-style) returns -1 if no byte is available
--- */
int _uartGetByte(uartObj* p) {
	if (!_uart_inited)
		return -1;
	return bufferGet(&(p->rxBuffer));
}

/* ---
int uartGetChar() - wait for an ASCII character (including white space) from the receive buffer and return it
--- */
int _uartGetChar(uartObj* p) {
	if (!_uart_inited)
		return -1;

	int c;
	do {
		c = bufferGet(&(p->rxBuffer));
		if (c < 0)
			return -1;
		// discard non printable 7bit characters
		if ((c & 0x80))
			c = -1;
		if (c < 0x20) {
			if (!((c == 0x00) || (c == 0x0A) || (c == 0x0D) || (c == 0x08) || (c == 0x09)))
				c = -1;
		}
	} while (c < 0);
	return c;
}

/* ---
int uartGetByteWait() - wait for a byte from the receive buffer and return it (the wait will time out after `UART_WAIT` (5 seconds) has expired)
--- */
extern uint32_t clockMillis();

int _uartGetByteWait(uartObj* p) {
	uint32_t timer = clockMillis() + UART_TIMEOUT;
	int b;
	while ((b = _uartGetByte(p)) < 0) {
		_delay_ms(1);
		if (clockMillis() > timer) {
			// TODO find all usage of this function and deal with the return
			break;
		}
	}
	return b;
}

#if 0
/* ---
int uartGetInteger(uint8_t max) - wait for up to `max` digit characters from the receive buffer or until a non-digit is received and then return the numerical value
--- */
int _uartGetInteger(uartObj* p, uint8_t len) {
	if (!_uart_inited)
		return -1;

	int value = -1;
	int c;
	uint8_t count = 0;

	// while (we have valid characters) or (we dont have a number yet)
	while (1) {
		c = bufferPeek(&(p->rxBuffer));
		if (c < 0) {
			_delay_ms(1);	// not sure if this is needed to prevent CPU starvation
			continue;
		}

		if ((c < '0') || (c > '9'))
			break;

		c = bufferGet(&(p->rxBuffer));
		c = c - '0';

		if (value < 0)
			value = 0;
		value = (value * 10) + c;

		// if the caller specified a length and we are read that many digits, we are done
		count++;
		if (len && (count >= len))
			break;
	}

	return value;
}
#endif

/* ---
#### UART Write Functions
--- */


/* ---
int uartPutByte(uint8_t txData) - transmit a byte
-- */
int _uartPutByte(uartObj* p, uint8_t txData) {

	if (!_uart_inited)
		return -1;

	int rtn = bufferPutWait(&(p->txBuffer), txData);

	if (rtn >= 0) {
		// if we have been idle, jump-start the transmission
		if (p->txIdle && p->txBuffer.length) {
			p->txIdle = false;
			//uint8_t b = (unsigned char)bufferGet(&(p->txBuffer));
			if (p->id == 0) {
				//UART.CTRLB |= (USART_TXEN_bm) | (USART_RXEN_bm);		// enable TX and RX
				UART_ENABLE_TX;
				// we need to manually load the first byte
				//UART_PORT.TXDATAL = b;
			}
#if IR_ENABLED
			else {
				//IR.CTRLB |= (USART_TXEN_bm) | (USART_RXEN_bm);		// enable TX and RX
				IR_ENABLE_TX;
				// we need to manually load the first byte
				//IR_PORT.TXDATAL = b;
			}
#endif
		}
	}
	else {
		// there was a problem
	}

	return rtn;
}

#if 0
/*	undocumented
uint8_t uartPutBufferPadded(char *buffer, uint16_t nBytes, uint16_t padded) - transmit `count` from `buffer` and if `count` is less that `padded` add trailing spaces
*/
uint8_t _uartPutBufferPadded(uartObj* p, char *buffer, uint16_t nBytes, uint16_t padded) {
	if (!_uart_inited)
		return true;
	if (!nBytes)
		return false;

	register uint16_t i;

	// comment out the follow line if you want to wait for buffer space
	//if (bufferCapacity(&uartTxBuffer) < nBytes) return false;

	// copy data to UART transmit buffer
	for (i = 0; i < nBytes; i++) {
		_uartPutByte(p, *buffer++); // bufferPutWait(&uartTxBuffer, *buffer++);
	}

	for (i = nBytes; i < padded; i++) {
		_uartPutByte(p, ' '); // bufferPutWait(&uartTxBuffer, ' ');
	}

	return true;
}

/* ---
bool uartPutStringPadded(char *buffer, uint16_t padded) - transmit a string and if its length is less than `padded` add trailing spaces
-- */
bool _uartPutStringPadded(uartObj* p, char *buffer, uint16_t padded) {
	uint16_t len = 0;
	if (buffer)
		len = strlen(buffer);
	return _uartPutBufferPadded(p, buffer, len, padded);
}

/* ---
bool uartPutString(char *buffer) - transmit a string
-- */
bool _uartPutString(uartObj* p, char *buffer) {
	return _uartPutStringPadded(p, buffer, 0);
}

/* ---
bool uartPutStringNL(char *buffer) - transmit a string and append a newline carrage return
-- */
bool _uartPutStringNL(uartObj* p, char *buffer) {
	bool rtn;
	rtn = _uartPutString(p, buffer);
	_uartPutString(p, "\n");
	return rtn;
}

/* ---
bool uartPutStringPNL(PGM_P buffer) - transmit a string from PROGMEM and append a newline carrige return
-- */
#if PROGMEM_ENABLED
bool _uartPutStringPNL(uartObj* p, PGM_P buffer) {
	char c;
	uint16_t index = 0;
	while ((c = pgm_read_byte(&(buffer[index]))) != 0) {
		_uartPutByte(p, c);
		index++;
	}
	_uartPutString(p, "\n");
	return true;
}
#else
#define _uartPutStringPNL(p, b) _uartPutStringNL(p, b)
#endif

/* ---
void uartPutNumberPadded(int16_t num, int8_t places, uint8_t pad_character) - transmit the necessary digit characters to represent a number, prepending the `pad_character` ( typically zero or space) to fill the required number of `places`.
-- */
void _uartPutNumberPadded(uartObj* p, int16_t num, int8_t places, uint8_t pad_char) {
	if (!_uart_inited)
		return;

	if (num < 0) {
		num = ABS(num);
		_uartPutByte(p, '-');
	}

	int16_t temp = num;
	int16_t i;
	bool render = false;
	int8_t digits = 5;	// initial digits in numbers >= 10000
	for (i = 10000; i > 0; i /= 10) {
		temp = num / i;

		if (temp > 0)
			render = true;
		if (digits <= places)
			render = true;

		// dont output leading zeros
		if (temp > 0)
			_uartPutByte(p, '0' + temp);
		else if (render && pad_char)
			_uartPutByte(p, pad_char);

		num -= (temp * i);
		digits--;
	}
}

/* ---
void uartPutNumber(int16_t num) - transmit the necessary digit characters to represent a number
-- */
void _uartPutNumber(uartObj* p, int16_t num) {
	_uartPutNumberPadded(p, num, 1, '0');
}

/* ---
void uartPutHexWord/Byte(int16_t num, bool add_symbol) - transmit the necessary hexadecimal characters to represent a number, prepending ` 0x` if `add_symbol` is true
-- */
void _uartPutHexWord(uartObj* p, uint16_t num, bool add_symbol) {
	if (!_uart_inited)
		return;

	if (add_symbol)
		_uartPutString(p, " 0x");

	uint16_t temp;
	uint8_t b;
	int8_t i;
	// 16 bits = 2 bytes = 4 hex characters
	for (i = (16-4); i >= 0; i -= 4) {
		temp = (num >> i) & 0xF;

		if (temp > 9)
			b = 'A' + (temp - 10);
		else
			b = '0' + (temp);
		_uartPutByte(p, b);

		num -= (temp << i);
	}
}
void _uartPutHexByte(uartObj* p, uint8_t num, bool add_symbol) {
	if (!_uart_inited)
		return;

	if (add_symbol)
		_uartPutString(p, " 0x");

	uint16_t temp;
	uint8_t b;
	int8_t i;
	// 8 bits = 1 bytes = 2 hex characters
	for (i = (8-4); i >= 0; i -= 4) {
		temp = (num >> i) & 0xF;

		if (temp > 9)
			b = 'A' + (temp - 10);
		else
			b = '0' + (temp);
		_uartPutByte(p, b);

		num -= (temp << i);
	}
}


/* ---
void uartPutBinaryLen(uint16_t num, uint8_t length, bool add_symbol) - transmit the specified number 0's and 1's characters to represent a number, prepending ` b` if `add_symbol` is true
-- */
void _uartPutBinaryLen(uartObj* p, uint16_t num, uint8_t len, bool add_symbol) {
	if (!_uart_inited)
		return;

	if (add_symbol)
		_uartPutString(p, " b");

	int8_t i;
	for (i = (len-1); i >= 0; i--) {
		uint8_t b = (num >> i) & 0x1;
		_uartPutByte(p, '0' + b);
	}
}

/* ---
void uartPutBinaryByte(uint8_t num, bool add_symbol) - transmit the necessary eight  0's and 1's characters to represent a number, prepending ` b` if `add_symbol` is true
-- */
void _uartPutBinaryByte(uartObj* p, uint8_t num, bool add_symbol) {
	_uartPutBinaryLen(p, num, 8, add_symbol);
}

/* ---
void uartPutBinaryWord(uint16_t num, bool add_symbol) - transmit the necessary sixteen  0's and 1's characters to represent a number, prepending ` b` if `add_symbol` is true
-- */
void _uartPutBinaryWord(uartObj* p, uint16_t num, bool add_symbol) {
	_uartPutBinaryLen(p, num, 16, add_symbol);
}
#endif


// ---------------- to insure developers know which UART they are using, the functions are made explicit -------------

void uartFlushReceiveBuffer(void) 											{	_uartFlushReceiveBuffer(&uart);								}
void uartFlushTransmitBuffer(void) 											{	_uartFlushTransmitBuffer(&uart);							}
uint8_t uartReceiveBufferOverflow() 										{	return _uartReceiveBufferOverflow(&uart); 					}
uint8_t uartReceiveBufferIsEmpty(void) 										{	return _uartReceiveBufferIsEmpty(&uart);					}
uint8_t uartTransmitBufferIsEmpty(void) 									{	return _uartTransmitBufferIsEmpty(&uart);					}
int uartGetByte(void) 														{	return _uartGetByte(&uart);									}
int uartGetChar(void) 														{	return _uartGetChar(&uart);									}
int uartGetByteWait(void) 													{	return _uartGetByteWait(&uart);								}
#if 0
int uartGetInteger(uint8_t len) 											{	return _uartGetInteger(&uart, len);							}
#endif
int uartPutByte(uint8_t txData) 											{	return _uartPutByte(&uart, txData);							}
#if 0
uint8_t uartPutBufferPadded(char *buffer, uint16_t nBytes, uint16_t padded)	{	return _uartPutBufferPadded(&uart, buffer, nBytes, padded);	}
bool uartPutStringPadded(char *buffer, uint16_t padded) 					{	return _uartPutStringPadded(&uart, buffer, padded);			}
bool uartPutString(char *buffer) 											{	return _uartPutString(&uart, buffer);						}
bool uartPutStringNL(char *buffer) 											{	return _uartPutStringNL(&uart, buffer);						}
bool uartPutStringPNL(PGM_P buffer) 										{	return _uartPutStringPNL(&uart, buffer);					}
void uartPutNumberPadded(int16_t num, int8_t places, uint8_t pad_char) 		{	_uartPutNumberPadded(&uart, num, places, pad_char);			}
void uartPutNumber(int16_t num) 											{	_uartPutNumber(&uart, num);									}
void uartPutHexByte(uint8_t num, bool add_symbol)							{	_uartPutHexByte(&uart, num, add_symbol);					}
void uartPutHexWord(uint16_t num, bool add_symbol)							{	_uartPutHexWord(&uart, num, add_symbol);					}
void uartPutBinaryByte(uint8_t num, bool add_symbol)						{	_uartPutBinaryByte(&uart, num, add_symbol);					}
void uartPutBinaryWord(uint16_t num, bool add_symbol)						{	_uartPutBinaryWord(&uart, num, add_symbol);					}
void uartPutBinaryLen(uint16_t num, uint8_t len, bool add_symbol)			{	_uartPutBinaryLen(&uart, num, len, add_symbol);				}
#endif
//void uartPutTouchData(uint16_t touches) 									{	_uartPutTouchData(&uart, touches);							}

void uartEnable()															{	UART_ENABLE_RX; UART_ENABLE_TX; }
void uartDisable()															{	UART_DISABLE_RX; UART_DISABLE_TX; }

#if IR_ENABLED

void irFlushReceiveBuffer(void) 											{	_uartFlushReceiveBuffer(&ir);								}
void irFlushTransmitBuffer(void) 											{	_uartFlushTransmitBuffer(&ir);								}
uint8_t irReceiveBufferOverflow() 											{	return _uartReceiveBufferOverflow(&ir); 					}
uint8_t irReceiveBufferIsEmpty(void) 										{	return _uartReceiveBufferIsEmpty(&ir);						}
uint8_t irTransmitBufferIsEmpty(void) 										{	return _uartTransmitBufferIsEmpty(&ir);						}
int irGetByte(void) 														{	return _uartGetByte(&ir);									}
int irGetChar(void) 														{	return _uartGetChar(&ir);									}
int irGetByteWait(void) 													{	return _uartGetByteWait(&ir);								}
#if 0
int irGetInteger(uint8_t len) 												{	return _uartGetInteger(&ir, len);							}
#endif
int irPutByte(uint8_t txData) 												{	return _uartPutByte(&ir, txData);							}
#if 0
uint8_t irPutBufferPadded(char *buffer, uint16_t nBytes, uint16_t padded)	{	return _uartPutBufferPadded(&ir, buffer, nBytes, padded);	}
bool irPutStringPadded(char *buffer, uint16_t padded) 						{	return _uartPutStringPadded(&ir, buffer, padded);			}
bool irPutString(char *buffer) 												{	return _uartPutString(&ir, buffer);							}
bool irPutStringNL(char *buffer) 											{	return _uartPutStringNL(&ir, buffer);						}
bool irPutStringPNL(PGM_P buffer) 											{	return _uartPutStringPNL(&ir, buffer);						}
void irPutNumberPadded(int16_t num, int8_t places, uint8_t pad_char) 		{	_uartPutNumberPadded(&ir, num, places, pad_char);			}
void irPutNumber(int16_t num) 												{	_uartPutNumber(&ir, num);									}
void irPutHexByte(uint8_t num, bool add_symbol)								{	_uartPutHexByte(&ir, num, add_symbol);						}
void irPutHexWord(uint16_t num, bool add_symbol)							{	_uartPutHexWord(&ir, num, add_symbol);						}
void irPutBinaryByte(uint8_t num, bool add_symbol)							{	_uartPutBinaryByte(&ir, num, add_symbol);					}
void irPutBinaryWord(uint16_t num, bool add_symbol)							{	_uartPutBinaryWord(&ir, num, add_symbol);					}
void irtPutBinaryLen(uint16_t num, uint8_t len, bool add_symbol)			{	_uartPutBinaryLen(&ir, num, len, add_symbol);				}
//void irPutTouchData(uint16_t touches) 										{	_uartPutTouchData(&ir, touches);							}
#endif
// unique function to the IR UART
void irEnable() {
#if IR_HIGH_POWER
	IR_PORT.OUTSET = IR_TX_PIN;
	IR_PORT.DIRSET = IR_TX_PIN;
#endif
	IR_ENABLE_RX;
	IR_ENABLE_TX;
	IR_PWR_PORT.DIRSET = IR_PWR_PIN;
	IR_PWR_PORT.OUTSET = IR_PWR_PIN;
}
void irDisable() {
#if IR_HIGH_POWER
	IR_PORT.OUTCLR = IR_TX_PIN;
	IR_PORT.DIRCLR = IR_TX_PIN;
#endif
	IR_DISABLE_RX;
	IR_DISABLE_TX;
	IR_PWR_PORT.DIRCLR = IR_PWR_PIN;
}

#else

#define irFlushReceiveBuffer()
#define irFlushTransmitBuffer()
#define irReceiveBufferOverflow()					(false)
#define irReceiveBufferIsEmpty()					(true)
#define irTransmitBufferIsEmpty()					(true)
#define irGetByte() 								(-1)
#define irGetChar() 								(-1)
#define irGetByteWait() 							(-1)
#define irGetInteger(len) 							(-1)
#define irPutByte(txData) 							(-1)
#define irPutBufferPadded(buffer, nBytes, padded)	(0)
#define irPutStringPadded(buffer, padded) 			(false)
#define irPutString(buffer) 						(false)
#define irPutStringNL(buffer) 						(false)
#define irPutStringPNL(buffer)						(false)
#define irPutNumberPadded(num, places, pad_char)
#define irPutNumber(num)
#define irPutHexByte(num, add_symbol)
#define irPutHexWord(num, add_symbol)
#define irPutBinaryByte(num, add_symbol)
#define irPutBinaryWord(num, add_symbol)
#define irtPutBinaryLen(num, len, add_symbol)
#define irPutTouchData(touches)

// unique function to the IR UART
#define irEnable()
#define irDisable()

#endif
