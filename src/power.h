/* ************************************************************************************
* File:	power.h
* Date:	2020.05.04
* Author:  Bradan Lane Studio
*
* This content may be redistributed and/or modified as outlined under the MIT License
*
* ************************************************************************************/

/* ---

## power.h - sleep / wake functions

**Note:** Because this code uses the timer established in `clock.h`,
this module must be included after including `clock.h`.

The power module provides sleep and wake capability as well as idle timeout detection.
It preserves all registers and timers during the sleep cycle.
The wake is detected from INT0.

--------------------------------------------------------------------------
--- */

#if !POWER_ENABLED

#define powerInit()
#define powerButtonSleepEnable()
#define powerButtonSleepDisable()
#define powerSleepUpdate()
#define powerSleepUpdateLong()
#define powerSleepNow()
#define powerButtonPressed() (false)
#define powerSleepConditionally() (false)

#else

#if (UART_RX_BUFFER_SIZE < (32 * 6)) // we need to save each PORT_t (32 bytes each) for 6 ports
#error THE UART BUFFER IS USED AS STORAGE DURING SLEEP AND IS TOO SMALL
#else
#define PORT_STORAGE uartRxData
#endif

extern uint32_t clockMillis();

uint32_t _power_sleep_timer;
bool _power_button_sleep;
#ifdef WARNING_MESSAGE
bool _power_warning_message_displayed;
#endif

/* ---
void powerInit() - perform necessary initialization of the power management subsystem
--- */
void powerInit() {
	_power_button_sleep = false;
	_power_sleep_timer = 0;
	_power_warning_message_displayed = false;

	WAKE_PORT.DIRCLR = WAKE_PIN;			 // Input: pin where a button is attached (pulled up)
	WAKE_PORT.WAKE_CTRL |= PORT_PULLUPEN_bm; // has internal pull-up
}

/* ---
void powerButtonSleepEnable/Disable() - allow sleep mode to be activated using the wake button
--- */
void powerButtonSleepEnable() {
	_power_button_sleep = true;
}

void powerButtonSleepDisable() {
	_power_button_sleep = false;
}

/* ---
void powerSleepUdate() - signal user action has occurred and reset the idle timeout
--- */
void powerSleepUpdate() {
	_power_sleep_timer = clockMillis() + NORMAL_SLEEP_DELAY;
#ifdef WARNING_MESSAGE
	_power_warning_message_displayed = false;
#endif
}

/* ---
void powerSleepUdateLong() - signal user action has occurred and reset the idle timeout for a long period
--- */
void powerSleepUpdateLong() {
	_power_sleep_timer = clockMillis() + LONG_SLEEP_DELAY;
#ifdef WARNING_MESSAGE
	_power_warning_message_displayed = false;
#endif
}

ISR(PORTA_PORT_vect) {
	// everything is pretty much handled in the sleep function (below)
	if (PORTA.INTFLAGS & WAKE_PIN) {
		PORTA.INTFLAGS &= WAKE_PIN;
	}
}

/* ---
void powerSleepNow() - begin sleep power shutdown process immediately; wake is triggered by INT0 being pulled low
--- */
void powerSleepNow() {
	ledsOff();
	soundOff();

#ifdef WARNING_MESSAGE
	_power_warning_message_displayed = false;
#endif
#ifdef SLEEP_MESSAGE
	printPrintf("%s\n", SLEEP_MESSAGE);
	_delay_ms(100);
#endif

	irDisable();
	uartDisable();

	// the following will save some power during power-down sleep
	ADC0.CTRLA = 0; // disable ADC
	AC0.CTRLA = 0;	// disable the analog comparator

	set_sleep_mode(SLEEP_MODE_PWR_DOWN);

	cli();

	// we need to save all PORTn data and then clear and/or reset it
	// WARNING: when porting to a new chip, insure all PORTn are contiguous and that PORTA is 0x0400
	memcpy(PORT_STORAGE, (void *)0x0400, sizeof(PORT_t) * 6);

#if defined(CHIP_AVR64DD32)
	PORTA.OUT = PORTC.OUT = PORTD.OUT = PORTF.OUT = 0;
	PORTA.DIR = PORTC.DIR = PORTD.DIR = PORTF.DIR = 0;
#else
	PORTA.OUT = PORTB.OUT = PORTC.OUT = PORTD.OUT = PORTE.OUT = PORTF.OUT = 0;
	PORTA.DIR = PORTB.DIR = PORTC.DIR = PORTD.DIR = PORTE.DIR = PORTF.DIR = 0;
	PORTB.PIN0CTRL = PORTB.PIN1CTRL = PORTB.PIN2CTRL = PORTB.PIN3CTRL = PORTB.PIN4CTRL = PORTB.PIN5CTRL = PORTB.PIN6CTRL = PORTB.PIN7CTRL =
	PORTE.PIN0CTRL = PORTE.PIN1CTRL = PORTE.PIN2CTRL = PORTE.PIN3CTRL = PORTE.PIN4CTRL = PORTE.PIN5CTRL = PORTE.PIN6CTRL = PORTE.PIN7CTRL =
#endif
	PORTA.PIN0CTRL = PORTA.PIN1CTRL = PORTA.PIN2CTRL = PORTA.PIN3CTRL = PORTA.PIN4CTRL = PORTA.PIN5CTRL = PORTA.PIN6CTRL = PORTA.PIN7CTRL =
	PORTC.PIN0CTRL = PORTC.PIN1CTRL = PORTC.PIN2CTRL = PORTC.PIN3CTRL = PORTC.PIN4CTRL = PORTC.PIN5CTRL = PORTC.PIN6CTRL = PORTC.PIN7CTRL =
	PORTD.PIN0CTRL = PORTD.PIN1CTRL = PORTD.PIN2CTRL = PORTD.PIN3CTRL = PORTD.PIN4CTRL = PORTD.PIN5CTRL = PORTD.PIN6CTRL = PORTD.PIN7CTRL =
	PORTF.PIN0CTRL = PORTF.PIN1CTRL = PORTF.PIN2CTRL = PORTF.PIN3CTRL = PORTF.PIN4CTRL = PORTF.PIN5CTRL = PORTF.PIN6CTRL = PORTF.PIN7CTRL =

	PORT_ISC_INPUT_DISABLE_gc;

	// NOTE: WAKE_PIN is our wake button when pressed (aka connected to GND)
	WAKE_PORT.DIRCLR = WAKE_PIN;			 // Input: pin where a button is attached (pulled up)
	WAKE_PORT.WAKE_CTRL |= PORT_PULLUPEN_bm; // has internal pull-up
	WAKE_PORT.WAKE_CTRL |= PORT_ISC_FALLING_gc;
	WAKE_PORT.WAKE_CTRL &= ~(PORT_ISC_INPUT_DISABLE_gc);

	sleep_enable();

	sei(); // ensure interrupts enabled so we can wake up again

	sleep_cpu(); // go to sleep

	// execution resumes here after wake from sleep

	cli();

	// WARNING: when porting to a new chip, insure all PORTn are contiguous and that PORTA is 0x0400
	memcpy((void *)0x0400, PORT_STORAGE, sizeof(PORT_t) * 6);

	sei();

	sleep_disable();

	uartInit();			// for some reason, this seems to not get restored correctly; odds are good other stuff is borked too
	touchInit();		// for some reason, this seems to not get restored correctly; odds are good other stuff is borked too

	powerSleepUpdate(); // reset the next sleep cycle

#ifdef WAKE_MESSAGE
	_delay_ms(100);
	printPrintf("\n%s\n", WAKE_MESSAGE);
#endif
}


/* ---
void powerButtonPressed() - expose the wake/sleep button so it is available for other uses.
A common use will be to skip a long process (such as a game puzzle output).
--- */
bool powerButtonPressed() {
	if (~(WAKE_PORT.IN) & WAKE_PIN) { // check if button pressed( aka pulled to GND)
		soundOff();

		ledsOn();
		_delay_ms(50);
		ledsOff();

		// wait for WAKE button to be released
		while (~(WAKE_PORT.IN) & WAKE_PIN) { // while button is PRESSED we wait
			_delay_ms(50);
		}
		_delay_ms(POWER_RELEASE_DELAY);	// give user a little time to release
		return true;
	}
	return false;
}

/* ---
void powerSleepConditionally() - begin sleep power shutdown process if the idle timer has expired.
This function should be called periodically from the main loop.
--- */
uint8_t powerSleepConditionally() {
	// if allowed to trigger sleep using the wake button, then check it here
	if (_power_button_sleep) {
		// check if WAKE button is pressed
		if (powerButtonPressed()) {
			// activate sleep
			powerSleepNow();
			return true;
		}
	}

	// if there has been no user action for a while, we go to sleep
	uint32_t now = clockMillis();
	if (now > _power_sleep_timer) {
		powerSleepNow();
		return true;
	} else if (now > (_power_sleep_timer - WARNING_SLEEP_DELAY)) {
#ifdef WARNING_MESSAGE
		if (!_power_warning_message_displayed) {
			_power_warning_message_displayed = true;
			printPrintf("%s\n", WARNING_MESSAGE);
		}
#endif
	}
	return false;
}

#endif