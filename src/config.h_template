/* ************************************************************************************
* File:    config.h
* Date:    2020.10.04
* Author:  Bradán Lane STUDIO
*
* This content may be redistributed and/or modified as outlined under the MIT License
* ************************************************************************************/

/* ---

# config.h - define all configuration for the specific ECC project using the ATMega4809 microcontroller

All of the ECC subsystems are configured from this one file. It is important to match the definitions
below with the electrical schematic for all used IO Pins.

## Config Notes:

The subsystems use two AVR timers:
 - the clock uses TCB0
 - the buzzer uses TCA0

The original ECCCore was built for the ATMega328PB. The intent was to make it generic and have
all configuration driven the a file like this one. Well, that was a good idea and if this project
had hundreds of users, it would be be warranted. However, it is really only used by Bradán Lane STUDIO
so the decision was to fork the ECCCore _(twice now)_ with the switch to AVR64x chips. _Sorry (again)._

The ECCCore has been tested with AVR64DA48 and AVR64DD32.

The newer Atmel chip syntax for pins and port operations provides both bit numbers (bp) and bit masks (bm).
This implementation of ECCCore now uses bit masks to simplify code. *Ironically, Atmel is inconsistent
within the individual chip headers (eg `iom4809.h`) using HEX mask values in some cases and using bit-shift macros in other cases.*

Example: `PORTA.OUT |= (1 << PIN0_bp);` is the same as `PORTA.OUTSET = PIN0_bm;`

### User Defined Power Output Pins:
To conserve power during sleep, all attached peripherals must also be powered down.
A simple solution is to power them from GPIO pins. This assumes the peripheral power
requirements do not exceed the output of an GPIO pin.

If a peripheral is only needed for a short time, vs anytime the device is awake, then
that peripheral should have it's own pin.

**TIP:** There are many `#if 0` code fragments with the subsystems to help with debugging.
Most rely on UART working. As a general bit of advice, the _Sound_ or _LEDs_ subsystems are the
easiest to get running. This alow for some amount of status/feedback. UART is the most useful.

A Hardware debugger such as the Atmel ICE in conjunction with Microchip Studio _(formerly Atmel Studio)_
is the quickest way to get the basics working. In a pinch, an oscilloscope on a GPIO pin can help debug timers.
--------------------------------------------------------------------------
--- */

/* ---
### CHIP Requirements:
The chip type drives some compiler preprocessing and is used to flag feature incompatibility.
Only one chip type may be defined. The various ISR vectors (interrupt handers) are an example of chip specific code.
--- */
//#define CHIP_AVE64DA48
//#define CHIP_AVE64DD32

/* ---
#### PROGMEM_ENABLED
PROGMEM is the ability to store char constants in program space.
--- */
#if (defined(CHIP_AVR64DA48) || defined(CHIP_AVR64DD32))
	#define PROGMEM_ENABLED 1
	#define STORYMEM PROGMEM
#else
	#error "MISSING CHIP_* DEFINITION"
#endif



/* ---
### Subsystem Enablement:
Most subsystems are optional and may be included/excluded as appropriate for the specific project.
--- */


/* ---
#### POWER_ENABLED
The power subsystem provides both the sleep/wait functions and the sleep/wake button. It is only zero for debugging
--- */
#define POWER_ENABLED	1


/* ---
#### EEPROM_ENABLED
EEPROM is the persistent storage of the microcontroller.
--- */
#define EEPROM_ENABLED	1


/* ---
#### UART_ENABLED
The I/O UART is implemented on UART0.
--- */
#define UART_ENABLED 1

/* ---
#### IR_ENABLED
The I/O UART is implemented on UART0. The IR transceiver uses a second UART.
--- */
#define IR_ENABLED	1


// chip specific validation
#ifndef CHIP_ATMEGA128RFA1
#if RF_ENABLED
#error "RF support is only available with the ATMega128RFA1 chip"
#endif
#endif



/* ---
#### LEDS_ENABLED
LEDs are charlieplexed and use a timer. Even if LEDs are not enabled, the timer is still used to provide
a clock source for measuring events. Timer TCB0 is used.
--- */
#define LEDS_ENABLED 1


/* ---
#### TOUCH_ENABLED
TOUCH sensors measure the relative capacitance on an IO pin. All sensors are updated in a single synchronous function.
--- */
#define TOUCH_ENABLED 1
#define TOUCH_POWERED 0 // old designed used a powered touch circuit; new design use a grounded circuit


/* ---
#### SOUND_ENABLED
The sound subsystem implements a one octave music scale consisting of 12 notes. The sound subsystem
assumes a piezoelectric buzzer on a specific pin to allow for using a hardware timer.
--- */
#define SOUND_ENABLED	1


/* ---
#### BUTTONS_ENABLED
The buttons subsystem provides convenience functions for checking the state of one or more buttons.
--- */
#define BUTTONS_ENABLED	0

// ---------------------------------------------------------------------------------------------------------------------
// end of subsystem enable definitions
// ---------------------------------------------------------------------------------------------------------------------






// ---------------------------------------------------------------------------------------------------------------------
// start of sybsystem configuration
// ---------------------------------------------------------------------------------------------------------------------

/* ---
## Fixed Purpose Pins:
Very few pins of the ATMega4809 have explicit meaning but several have default capabilities
and are used within the ECCCore. It is important to uses these pins appropriately within
the project schematic. Most of the relevant restrictions are outlined in
the _I/O Multiplexing and Considerations_ table found in the microcontroller datasheet.
--- */



#if (UART_ENABLED || IR_ENABLED)
// ---------------------------------------------------------------------------------------------------------------------
/* ---
The USARTs on the AVR64x chips use PIN0 and PIN1 on PORTA, PORTC, PORTF, and PORTB corresponding to USART0 .. USART3.
Each USART also supports an alternate pin assignment using PIN4 and PIN5 on the same ports.
The code in `uart.h` assume the primary pin assignments are used.
--- */

/* ---
### I/O Buffers:
**Assumptions:** The transmitting of data can be blocking if there is insufficient space to queue data into the TX buffer.
The receive buffer often needs to be larger to accommodate incoming data when other ECC activity delays pulling data off the RX buffer.
When also using the IR UART, it is not necessary they have the same buffer sizes if their purpose is significantly different.
When also using the RF, the TX buffer is limited by the size of the transceiver FRAME with is 127+1 bytes.
--- */

#if UART_ENABLED
#define UART_BAUD 			9600
#define UART 				USART0
#define UART_PORT			PORTA
#define TX_PIN				PIN0_bm
#define RX_PIN				PIN1_bm
#define UART_TX_INTERRUPT	USART0_DRE_vect
#define UART_RX_INTERRUPT	USART0_RXC_vect

#define UART_TX_BUFFER_SIZE 0x080
#define UART_RX_BUFFER_SIZE 0x100
#endif

#if IR_ENABLED
#define IR_BAUD 			1200
#define IR 					USART1
#define IR_PORT				PORTC
#define IR_TX_PIN			PIN0_bm
#define IR_RX_PIN			PIN1_bm
#define IR_TX_INTERRUPT		USART1_DRE_vect
#define IR_RX_INTERRUPT		USART1_RXC_vect

#define IR_TX_BUFFER_SIZE 0x040
#define IR_RX_BUFFER_SIZE 0x040

/* ---
The IR circuit forms a hardware level UART. The photo-transistor requires power for its pull up.
Using a pin, rather than VCC, allows the circuit to be powered down to conserve battery
during deep sleep.
--- */
#define IR_PWR_PORT			PORTC
#define IR_PWR_PIN			PIN3_bm	// feed power to the photo-transistor
#endif


// the BAUD rate formula varies by chip architecture
#define BAUD_RATE_FORMULA(b) ((((8 * F_CPU) / b) + 1) / 2)
//#define BAUD_RATE_FORMULA(b) ((4 * F_CPU) / (b))	// ( (64 * F_CPU) / (16 * (b)) ) ==> where 16 is the samples per bit
// ---------------------------------------------------------------------------------------------------------------------
#endif



#if POWER_ENABLED
// ---------------------------------------------------------------------------------------------------------------------
/* ---
The wake from sleep uses a single GPIO pin
--- */
#define WAKE_PORT			PORTA
#define WAKE_PIN			PIN7_bm
#define WAKE_CTRL			PIN7CTRL

/* ---
### Power Idle Timeouts:
The power-down sleep capability can act on idle detection. There are twe functions to quickly indicate interaction and reset the idle timeout.
The `NORMAL_SLEEP_DELAY` and `LONG_SLEEP_DELAY` definitions are used to control defaults for idle timeout.
--- */

#define NORMAL_SLEEP_DELAY		 60000	// 60 seconds
#define LONG_SLEEP_DELAY		600000	// 10 minutes
#define WARNING_SLEEP_DELAY		 10000	// 10 seconds
#define POWER_RELEASE_DELAY		   250	// very short for physical power button; longer if using a pair of contact pads

/* ---
#### SLEEP_MESSAGE_ENABLED
It can be handy to display a message when entering sleep mode and when exiting sleep mode.

IMPORTANT: don't forget to create defines for SLEEP_MESSAGE and WAKE_MESSAGE (below)
--- */

#define SLEEP_MESSAGE_ENABLED 1

/* ---
### SLEEP_MESSAGE Setup:
The sleep / wake / WARNING messages are not required. If any of the the `SLEEP_MESSAGE`, `WAKE_MESSAGE`, or  `WARNING_MESSAGE` definitions and not set, the corresponding code will be optimized away.
--- */

#if SLEEP_MESSAGE_ENABLED
#define WARNING_MESSAGE	"sleep in 10 seconds\n"
#define SLEEP_MESSAGE 	"sleeping now\n"
#define WAKE_MESSAGE  	"waking up\n"
#endif
// ---------------------------------------------------------------------------------------------------------------------
#endif



#if SOUND_ENABLED
// ---------------------------------------------------------------------------------------------------------------------
/* ---
The sound subsystem uses the TCA0 hardware timer.
The AVR64DD32 has limited pins. To accommodate two UARTs, the BUZZER uses one of the alternative positions for TCA0.
If WO0 is not used, then an alternate timer mode is also needed.
--- */
#define BUZZER_PORT			PORTC
#define BUZZER_PORT			PORTF
#define BUZZER_PIN			PIN2_bm
// ---------------------------------------------------------------------------------------------------------------------
#endif




// ---------------------------------------------------------------------------------------------------------------------

/* ---
### User Defined IO Pins:
Several general IO pins are needed within the core. It is important to match the definitions with the project schematic.
--- */


/* ---
An ECC project may have buttons or interactions connected to pins.
Each button requires a port and a pin assignment. Currently all buttons are assumed pressed when LOW.
--- */

#if BUTTONS_ENABLED
#define BUTTON_COUNT 1
PORT_t* _button_ports[BUTTON_COUNT] = { &PORTA };
uint8_t _button_pins[BUTTON_COUNT] = { PIN7_bm };
uint8_t* _button_ctrls[BUTTON_COUNT] = { &(PORTA.PIN7CTRL) };

// optionally give names to buttons to improve code readability; these names are not used in teh sybsystem
#define BUTTON_1 0

// ---------------------------------------------------------------------------------------------------------------------
#endif


#if LEDS_ENABLED
// ---------------------------------------------------------------------------------------------------------------------

/* ---
### LED IO Pins:
Discrete LEDs should be charlieplexed to reduce pin usage.
Regardless of charlieplexing, LEDs are be multiplexed to reduce power usage.

Charlieplexed LEDs require 2 pins for 2 LEDs; 3 pins for 6 LEDs; 4 pins for 12 LEDs; 5 pins for 20 LEDs; and 6 pins for 30 LEDs.

The code assumed all LED pins are within a single IO PORT.
RGB LEDs may have their `color` cathode pins in a separate PORT as the LED anode pins.

As the number of LEDs increases, the use of brightness _levels_ is discouraged.
When considering the number of LEDs, each discrete LED and each RGB LED counts as 1.

**WARNING: The LED implementation has changed and is not backward compatible !!!**

LEDs are now defined using a 16bit value which combines pin information, brightness levels, and optionally color (for RGB LEDs)

Each LED has the following representation: 0bRxHHHLLL 0bxEBBBCCC (where 'x' is a reserved/unused bit)
 - the first block is the 'pins' byte (which is constant) and is broken into three numbers:
      - R is a single bit indicating the LED is RGB vs a discrete color LED
      - HHH is a value 0..7 representing the anode (HIGH) pin
	  - LLL is a value 0..7 representing the cathode (LOW) pin
 - the second byte (runtime state) is broken into three numbers:
      - E is a single bit indicating the LED is enabled/disabled
	  - BBB is a value 0..7 representing brightness (limited to the value of LED_LEVELS)
	  - CCC is a value 0..7 representing color (ignored for 'D' discrete LEDs)

--- */

typedef uint8_t LEDCONFIG;
typedef uint8_t LEDSTATE;

#define LEDS_COUNT		4			// 4 LEDs
#define LED_LEVELS		4			// brightness is 0, 1, 2, or 3; use 0 for ON/OFF


#define LED_PORT 		PORTD
#define LED_PIN1 		PIN4_bm
#define LED_PIN2 		PIN5_bm
#define LED_PIN3 		PIN6_bm
#define LED_PIN4 		PIN7_bm
#define ALL_LED_PINS ((LED_PIN1) | (LED_PIN2) | (LED_PIN3) | (LED_PIN4))


#define RGB_LEDS		1			// set to 0 if all LEDs are discrete; this will optimize the code ... a little bit
#if RGB_LEDS
#define RGB_PORT		PORTA		// ignored if RGB_LEDS is 0
#define BLUE_PIN		PIN4_bm		// ignored if RGB_LEDS is 0
#define GREEN_PIN		PIN5_bm		// ignored if RGB_LEDS is 0
#define RED_PIN			PIN6_bm		// ignored if RGB_LEDS is 0
#define ALL_COLOR_PINS ((RED_PIN) | (GREEN_PIN) | (BLUE_PIN))
#endif

// the order LEDs are sequenced and referenced - based on their physical location - may want to be different than the order they are defined
const uint8_t _leds_order[LEDS_SEQUENCE] 	= {0, 1, 2, 3};
const uint8_t _leds_game_order[] 			= {0, 1, 2, 3};

// a bunch of helper macros to make working with LED data easier

// _MAKE_LEDDATA, _DISCRETE_LEDCONFIG, and _RGB_LEDDATA create an LEDDATA item
#define _MAKE_LEDCONFIG(h, l, r)		( (((r) & 0x01) << 7) | (((h) & 0x07) << 3) | (((l) & 0x07)) )
#define _MAKE_LEDSTATE (e, b, c)		( (((e) & 0x01) << 6) | (((b) & 0x07) << 3) | (((c) & 0x07)) )

#define _DISCRETE_LEDCONFIG(h, l)		_MAKE_LEDCONFIG((h), (l), (0))
#define _RGB_LEDCONFIG(h)				_MAKE_LEDCONFIG((h), (0), (1))

// these definitions map color bits
#define COLOR_RED		0b00000001
#define COLOR_GREEN		0b00000010
#define COLOR_YELLOW	0b00000011
#define COLOR_BLUE		0b00000100
#define COLOR_MAGENTA	0b00000101
#define COLOR_CYAN		0b00000110
#define COLOR_WHITE		0b00000111

const uint8_t _led_pins[LEDS_COUNT] = {LED_PIN1, LED_PIN2, LED_PIN3, LED_PIN4};	// a pin is 8 bits

/* Here is an example of 4 RGB LEDs; the paramater is the index of _leds_pins */
const LEDCONFIG _leds_config[LEDS_COUNT] = {
	_RGB_LEDCONFIG(0),
	_RGB_LEDCONFIG(1),
	_RGB_LEDCONFIG(2),
	_RGB_LEDCONFIG(3)
	};

/* Here is an example of 12 discrete LEDs (12 LEDs is the max from charlieplexing 4 pins)
LEDCONFIG _leds_config[LEDS_COUNT] = {
	_DISCRETE_LEDCONFIG(0, 1),
	_DISCRETE_LEDCONFIG(1, 0),
	_DISCRETE_LEDCONFIG(0, 2),
	_DISCRETE_LEDCONFIG(2, 0),
	_DISCRETE_LEDCONFIG(0, 3),
	_DISCRETE_LEDCONFIG(3, 0),
	_DISCRETE_LEDCONFIG(1, 2),
	_DISCRETE_LEDCONFIG(2, 1),
	_DISCRETE_LEDCONFIG(1, 2),
	_DISCRETE_LEDCONFIG(3, 1),
	_DISCRETE_LEDCONFIG(2, 3),
	_DISCRETE_LEDCONFIG(3, 2)
};
*/

LEDSTATE _leds_state[LEDS_COUNT];


// helpers for access the led configuration
#define LED_IS_RGB(n)			(          (_leds_config[(n)%LEDS_COUNT] & 0b10000000)? true:false)
#define LED_HIGH_PIN(n)			(_led_pins[(_leds_config[(n)%LEDS_COUNT] & 0b00111000) >> 3])
#define LED_LOW_PIN(n)			(_led_pins[(_leds_config[(n)%LEDS_COUNT] & 0b00000111)])

// helpers for access the led state
#define CLEAR_LED(n)			_leds_state[(n)%LEDS_COUNT] = 0
#define SET_LED(n, e, b, c)		_leds_state[(n)%LEDS_COUNT] = (((e) & 0x01)<< 6) | (((b) & 0x07)<< 3) | (((c) & 0x07))
#define ENABLE_LED(n)			_leds_state[(n)%LEDS_COUNT] |=  0b01000000
#define DISABLE_LED(n)			_leds_state[(n)%LEDS_COUNT] &=  0b10111111

#define LED_IS_ENABLED(n)			((_leds_state[(n)%LEDS_COUNT] & 0b01000000)? true:false)
#define LED_BRIGHTNESS(n)		((_leds_state[(n)%LEDS_COUNT] & 0b00111000) >>  3)
#if RGB_LEDS
#define LED_COLOR_PINS(n)		(((_leds_state[(n)] & 0b00000001)? RED_PIN:0) | ((_leds_state[(n)] & 0b00000010)? GREEN_PIN:0) | ((_leds_state[(n)] & 0b00000100)? BLUE_PIN:0))
#else
#define LED_COLOR_PINS(n) (0)
#endif


/* ---
#### LED_ANIMATIONS
Animations are defined as a series of _frames_ where each frame is a complete copy of _leds_state.
The full animation is:
 - N the number of frames
 - T the duration of a full cycle of the animation
 - D an array of _leds_state (aka an array of arrays = LED_COUNT * N frames)

As you can see, LED animations are use significant memory. As an example, with 5 RGB LEDs and animation consisting of 9 frames
the data storage is 5 * 9  bytes = 45 bytes !YIKES!

The should be declared as `const` to store them in program memory.
--- */

// ---------------------------------------------------------------------------------------------------------------------
#endif



#if TOUCH_ENABLED
// ---------------------------------------------------------------------------------------------------------------------
/* ---
### Touch Sensor IO Pins:
Touch sensors require a specific project schematic and PCB design.

The touch sensing is performed synchronously.

With only a single PORT, the maximum number of touch sensors is 8, but this only is possible when using a PORT with PIN0 thru PIN7 available.
Some PORTs have fewer available pins.

The code had been expanded - *and made a bit more complex* - to handle touch sensors across two PORTS. It would be smaller code and
slightly more efficient if all sensors were within a single AVR PORT. When `TOUCH_PORTS_2X` is ZERO, the code is optimized at compile time.
--- */

#define TOUCH_PORTS_2X 0

/* ---
There are many different ways to define the Touch sensors. The minimum definitions and declarations include:
- `TOUCH_PORT_n?`
- `TOUCHn_PIN`
- `TOUCH_COUNT`
- `TOUCH_SPLIT` (when two ports are used)

The calibration of the touch sensors starts with a default: `TOUCH_DEFAULT_SINGLE_THRESHOLD`.
The touch sensors will attempt to self-calibrate.
If EEPROM is enabled and the `EEPROM_TOUCH_CALIBRATION` location offset is defined, then calibration will persist.

**NOTES:** It is rare that `TOUCH_MEASUREMENT_ITERATIONS` or `TOUCH_UPPER_LIMIT` need to be changed but there may
be cases where the touch range needs greater latitude.

The threshold and readings are based on the time it takes the code to test and loop.
For this reason, the values are affected by the MCU clock speed `F_CPU`. The initial values provided  here are based on tests
with a MCU running at 2MHz.

The default behavior is to treat touch sensors as switches. However, using `touchValue()` is is possible to react to the touch level.
--- */

#define TOUCH_PORT_0 PORTD

#if TOUCH_PORTS_2X
#define TOUCH_PORT_1 PORTE
#define TOUCH_SPLIT 4		// if sensors span 2 ports, this defines the index of the switch point
#endif

#define TOUCH1_PIN	PIN3_bm
#define TOUCH2_PIN	PIN2_bm
#define TOUCH3_PIN	PIN1_bm
#define TOUCH4_PIN	PIN0_bm

#define TOUCH_COUNT 3

// add defines - if you want - to make code more readable
#define TOUCH_PAD_1 0
#define TOUCH_PAD_2 1
#define TOUCH_PAD_3 2
#define TOUCH_PAD_4 3

#define TOUCH_MEASUREMENT_ITERATIONS	3	// number of samples taken and then averaged together to give a usable measurement
#define TOUCH_DEFAULT_SINGLE_THRESHOLD	15	// if we don't have any calibration data then we need a default
#define TOUCH_DEFAULT_THRESHOLD			(TOUCH_DEFAULT_SINGLE_THRESHOLD * TOUCH_MEASUREMENT_ITERATIONS)
#define TOUCH_UPPER_LIMIT				64	// needs to greater than threshold and less that 256; set to 256 to disable; higher values would allow for variable touch data

const uint8_t _touch_pins[TOUCH_COUNT] = {TOUCH1_PIN, TOUCH2_PIN, TOUCH3_PIN /*, TOUCH4_PIN */};

// ---------------------------------------------------------------------------------------------------------------------
#endif



#if EEPROM_ENABLED
// ---------------------------------------------------------------------------------------------------------------------
/* ---
### EEPROM Setup:
No specific configuration setup is *required* for the EEPROM. However, for `format` detection to work, a user defined bytecode
and location is required. These are optional definitions and the code will optimize away format detection if these are not defined.

If the `FORMAT_MARKER` definitions are present, the eeprom *format* related functions are useful for storing initial values in EEPROM.

*Note:* The ATMega4809 has only 256 bytes of EEPROM and the functions take use a relative address space starting at 0x0000
--- */

// ---------------------------------------------------------------------------------------------------------------------

// all of the following are unsigned byte data entries in the eeprom space
#ifndef EEPROM_FORMAT_MARKER_VALUE
#define EEPROM_FORMAT_MARKER_VALUE	0x01	// any byte code other than 0x00 and 0xFF
#endif
#ifndef EEPROM_FORMAT_MARKER
#define EEPROM_FORMAT_MARKER  		0x0000 // any location less than the MAX of the microcontroller EEPROM space
#endif
#ifndef EEPROM_TOUCH_CALIBRATION
#define EEPROM_TOUCH_CALIBRATION	0x0010 // up to 16 touch sensors; if 8 or less, then they could go in the 0x0000-0x0010 block
// this end location of critical for locating any other storage
#endif
#ifndef EEPROM_ID_STORAGE
#define EEPROM_ID_STORAGE	  		0x0020 // first slot used for storing received coin IDs (0x0100 - 0x0020 = 224; IDs are 6 bytes so 224 / 6 = 37 IDs storage
#endif
// ---------------------------------------------------------------------------------------------------------------------
#endif




// ---------------------------------------------------------------------------------------------------------------------
// minimize printf size
#define PRINTF_DISABLE_SUPPORT_FLOAT
#define PRINTF_DISABLE_SUPPORT_EXPONENTIAL
#define PRINTF_DISABLE_SUPPORT_LONG_LONG
#define PRINTF_DISABLE_SUPPORT_PTRDIFF_T
#define PRINTF_DISABLE_PRECISION
#define PRINTF_MINIMIZE_CODE	// note: this eliminates some formatting options
#define PRINTF_DISABLE_PRECISION
// ---------------------------------------------------------------------------------------------------------------------

