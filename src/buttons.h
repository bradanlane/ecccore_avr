/* ************************************************************************************
* File:    button.h
* Date:    2020.10.04
* Author:  Bradan Lane Studio
*
* This content may be redistributed and/or modified as outlined under the MIT License
*
* ************************************************************************************/

/* ---

## button.h - simple button handling functions

A button is defined by a port and a pin.
The code is simple and assumes a button is pressed when its value is LOW.

Buttons are referenced by their index in the button table (defined in config.h).

For code readability, names may be given to the indexes - *as detailed on `config.h`.

**NOTE:** the button code does not provide any extended capabilities such as
detecting short vs long press. This logical is left as an exercise for the reader.
--------------------------------------------------------------------------
--- */

#if !BUTTONS_ENABLED

#define buttonInit() {}
#define buttonPressed(n) (false)

#else

bool _buttons_inited;

/* ---
void buttonInit() - called once to set teh array of buttons to INPUT
and enable the internal pullup resistors.
A common use will be to skip a long process (such as a game puzzle output).
--- */

void buttonInit() {
	for (int i = 0; i < BUTTON_COUNT; i++) {
		(_button_ports[i])->DIRCLR = _button_pins[i];	// make button pin as input
		// enable internal pullup
		*(_button_ctrls[i]) |= PORT_PULLUPEN_bm; // has internal pull-up
	}
	_buttons_inited = true;
}

/* ---
bool buttonPressed(uint8_t index) - return true if the button is currently pressed
--- */
bool buttonPressed(uint8_t n) {
	if (!_buttons_inited) return false;
	if (n >= BUTTON_COUNT) return false;

 	/* check if button pulled to GND */
	if (~((*_button_ports[n]).IN) & _button_pins[n])
		return true;
	return false;
}

#endif // end BUTTONS_ENABLED
