/* ************************************************************************************
* File:    leds.h
* Date:    2020.05.04
* Author:  Bradan Lane Studio
*
* This content may be redistributed and/or modified as outlined under the MIT License
*
* ************************************************************************************/

/* ---

## leds.h - control a set of charlieplexed LEDs using the timer establish in `clock.h`

**Note:** Because this code integrates into the timer established in `clock.h`,
this module must be included before including `clock.h`.

While only a single LED is every ON at any instant, the use of the timer allows for the appearance
of any number of LEDs to be ON at any time. This conserves power and enables the use of charlieplexing
to minimize the number of pins needed for LEDs.

**Tip:** Charlieplexing require 2 pins for 2 LEDs; 3 pins for 6 LEDs; 4 pins for 12 LEDs; 5 pins for 20 LEDs; and 6 pins for 30 LEDs.

--------------------------------------------------------------------------
--- */

#if !LEDS_ENABLED

#define _leds_timer_event() {}
#define ledsInit() {}
#define ledsIsInited() (0)
#define ledOn(num) {}
#define ledOff(num) {}
#define ledBlink(num) {}
#define ledsOn() {}
#define ledsOff() {}
#define ledsSequencer(speed) {}
#define ledsAnimationStart(duration, count, sequence) {}
#define ledsAnimationStop() {}
#define ledsDisable() {}
#define ledsEnable() {}

#else

/*
LEDs are charlieplexed to

  (1) allow additional LEDs in the future with minimal added pins
  (2) prevent multiple LEDs from drawing power at the same time

With a timer running at 2KHz, and a minimum complete refresh rate of 50 times per second, we could handle
  up to 10 LEDs with 4 levels of brightness
  up to 40 LEDs with on/off
*/

#define LED_TIMER_FREQ 2000		// HACK This is actually defined in clock.h but that gets included after leds.h
#define LED_TICKS_PER_MILLI (LED_TIMER_FREQ / 1000)

/*
	NOTE!!! when LED_LEVELS is '1' then no BRIGHTNESS related code is used in favor of on/off performance.

	Example: number of ticks to maintain 50Hz refresh for the entire set of LEDs
	eg: with 2000 ticks/second and 10 LEDs, this is a refresh rate of 50Hz for every LED
*/
#define LED_TICKS ((LED_TIMER_FREQ) / ((LEDS_COUNT) * (LED_LEVELS) * 50))

uint8_t _leds_inited;
uint8_t _led_enabled;

uint16_t _led_animation_counter;
uint16_t _led_animation_frame_ticks;
uint8_t  _led_animation_frame;
uint8_t  _led_animation_frames;
ANIMATIONFRAME **_led_animation_sequence;

// the follow variables are used within the interrupt to change which set of LEDs is active
volatile uint8_t	_led_tick_counter;			// number of ticks between 'actions'
volatile uint16_t	_led_num;					// which LED is active
volatile int8_t		_led_brightness_counter;	// number of available levels of brightness
volatile int8_t		_led_brightness;			// LED brightness

/*
LEDs are charlieplexed to

  (1) allow additional LEDs in the future with minimal added pins
  (2) prevent multiple LEDs from drawing power at the same time

  up to 8 LEDs can support 16 levels of brightness
  up to 30 LEDs can support 4 levels of brightness
*/

// using Timer0 for LEDs; TCCR0 is an 8-bit timer

// set output direction and port pin to turn on an LED
// only one LED is on at any time

void _charlieplex_all_off() {
#if RGB_LEDS
	RGB_PORT.DIRCLR = (ALL_COLOR_PINS);	// all as input
	LED_PORT.OUTCLR = (ALL_COLOR_PINS);	// all low
	// colors need to be set to out.low
#endif
	LED_PORT.DIRCLR = (ALL_LED_PINS);	// all as input
	LED_PORT.OUTCLR = (ALL_LED_PINS);	// all low
}

void _charlieplex_led_on(uint16_t led) { 	// led_num must be in range for a zero-based index
	//_charlieplex_all_off();
	uint8_t pins;

#if RGB_LEDS
	if (LED_IS_RGB(led)) {
		pins = LED_COLOR_PINS(led);
		RGB_PORT.DIRSET = pins;				// color pins as OUT
		RGB_PORT.OUTCLR = pins;				// color pins as LOW
		pins = LED_HIGH_PIN(led);
		LED_PORT.DIRSET = pins;				// LED pin as OUT
		LED_PORT.OUTSET = pins;				// LED pin as HIGH
	} else
#endif
	{
		pins = LED_HIGH_PIN(led);			// LED pin as HIGH
		LED_PORT.OUTSET = pins;
		pins |= LED_LOW_PIN(led);			// both LEDs pin as OUT
		LED_PORT.DIRSET = pins;
	}
}

static inline void _leds_timer_event() {
	if (!_leds_inited || !_led_enabled) return;

	// cycle through LED's really really fast so it looks like any number of LEDs are on at the same time
	_led_tick_counter = (_led_tick_counter + 1) % LED_TICKS;
	// only make changes when the _led_tick_counter wraps around to zero
	if (_led_tick_counter)
		return;

	_led_brightness_counter--;

	if (_led_brightness <= 0)
		_charlieplex_all_off();
	_led_brightness--;

	// if we are in an animation, we index frame after the specified number of ticks
	if (_led_animation_frames) {
		if (_led_animation_counter == 0) {
			// TODO do we need to preserve/restore the _leds_data[]?
			// copy next animation frame into the _leds_data ...
			memcpy(_leds_state, &(_led_animation_sequence[_led_animation_frame * LEDS_COUNT]), (LEDS_COUNT * sizeof(LEDSTATE)));
			_led_animation_frame = (_led_animation_frame + 1) % _led_animation_frames;
		}
		_led_animation_counter = (_led_animation_counter + 1) % _led_animation_frame_ticks;
	}

	if (_led_brightness_counter <= 0) {
		_led_num = (_led_num + 1) % LEDS_COUNT;	// time to move to teh next LED ...
		_led_brightness_counter = LED_LEVELS;	// reset the number of iterations per LED

		_led_brightness = 0;
		if (LED_IS_ENABLED(_led_num)) {
			_led_brightness = LED_BRIGHTNESS(_led_num);
			_charlieplex_led_on(_led_num);
		}
	}
}



/* ---
void ledsInit() - called to initialize the LEDs internal state structures and set all LEDs to OFF
--- */
void ledsInit() {
	// initialize all of the internal data before starting the interrupt handler

	_led_enabled = true;
	_leds_inited = true;

	_led_tick_counter = 0;
	_led_brightness_counter = LED_LEVELS;

	_led_num = 0;
	_led_brightness = 0;

	for (int i = 0; i < LEDS_COUNT; i++) {
		CLEAR_LED(i);
	}
}

/* ---
bool ledsInited() - returns true is the LEDs module has been initialized
--- */
bool ledsIsInited() {
	return _leds_inited;
}



/* ---
void ledSet(uint8_t num, uint8_t brightness, uint8_t color_index) - set LED color and brightness data
--- */
void ledSet(uint8_t num, bool on, uint8_t brightness, uint8_t color_index) {
	if (!_leds_inited) return;
	num = num % LEDS_COUNT;
	if (on) on = 1;
	SET_LED(num, on, brightness, color_index);
}

/* ---
void ledOn(uint8_t num) - turn on an individual LED; it will remain on until turned off directly or when turning off all LEDs
--- */
void ledOn(uint8_t num) {
	if (!_leds_inited) return;
	num = num % LEDS_COUNT;
	ENABLE_LED(num);
}

/* ---
void ledRgbOn(uint8_t num, uint8_t color) - turn on an individual LED; it will remain on until turned off directly or when turning off all LEDs
--- */
void ledRgbOn(uint8_t num, uint8_t color) {
	if (!_leds_inited) return;
	num = num % LEDS_COUNT;
	color = color % 7;
	ledSet(num, true, LED_LEVELS - 1, _leds_colors[color]);
}

/* ---
void ledOff() - turn off an individual LED
--- */
void ledOff(uint8_t num) {
	if (!_leds_inited) return;
	num = num % LEDS_COUNT;

	DISABLE_LED(num);
}

/* ---
void ledBlink(uint8_t num) - turn on and then off an LED one time very quickly - useful for testing
--- */
void ledBlink(uint8_t num) {
	if (!_leds_inited) return;

	ledOn(num);
	_delay_ms(10);
	ledOff(num);
}


/* ---
void ledsOn() - turn on all LEDs
--- */
void ledsOn() {
	if (!_leds_inited) return;

	for (int i = 0; i < LEDS_COUNT; i++) {
		ENABLE_LED(i);
	}
}

/* ---
void ledsOff() - turn off all LEDs
--- */
void ledsOff() {
	if (!_leds_inited) return;

	for (int i = 0; i < LEDS_COUNT; i++) {
		DISABLE_LED(i);
	}
}

/* ---
void ledsMultiOn() - turn on LEDs based on a bit mask
--- */
void ledsMultiOn(uint16_t mask) {
	for (int i = 0; i < LEDS_COUNT; i++) {
		if (mask & (0x1 << i)) {
			ENABLE_LED(i);
		}
	}
}

/* ---
void ledsSequencer(uint8_t speed) - animate the LEDs once time in the sequence defined by the `_leds_order[]` array
--- */
void ledsSequencer(uint8_t speed) {
	for (int i = 0; i < LEDS_SEQUENCE; i++) {
		ledOn(_leds_order[i]);
		for (uint8_t d = 0; d < speed; d++)
			_delay_ms(50);
	}
	_delay_ms(50);
	for (int i = 0; i < LEDS_SEQUENCE; i++) {
		ledOff(_leds_order[(LEDS_SEQUENCE-1) - i]);
		for (uint8_t d = 0; d < speed; d++)
			_delay_ms(50);
	}
}

/* ---
void ledsAnimationStart(uint16_t duration, uint8_t count, ANIMATIONFRAME sequence[])

Load and start an animation which will take `duration` to complete one full cycle.
The animation has `count` frames.
The `sequence` is an array of `count` frames.
Each `frame` is an ANIMATIONFRAME.
Thus, the total size of `sequence` data is `ANIMATIONFRAME[count]`.
--- */
void ledsAnimationStart(uint16_t duration, uint8_t count, ANIMATIONFRAME **sequence) {
	_led_animation_counter = 0;
	_led_animation_frame_ticks = LED_TICKS_PER_MILLI * duration / count / LED_TICKS;
	_led_animation_frames = count;	// number of frames
	_led_animation_sequence = sequence;	// MUST be LEDSTATE array of size `count` * LEDS_COUNT
	ledsOff();
}

/* ---
void ledsAnimationStop() - end an active animation and return the LEDs to normal state operation using `ledOn()` etc.
--- */
void ledsAnimationStop() {
	_led_animation_counter = 0;
	_led_animation_frame_ticks = 0;
	_led_animation_frames = 0;
	_led_animation_sequence = NULL;
	ledsOff();
}


/* ---
void ledsAnimationSpeed(uint16_t duration) - change speed of active animation
--- */
void ledsAnimationSpeed(uint16_t duration) {
	if (_led_animation_frames) {
		_led_animation_frame_ticks = LED_TICKS_PER_MILLI * duration / _led_animation_frames / LED_TICKS;
	}
}

/* ---
uint_t ledsAnimationFrame() - get current frame (this is a moving target :-O )
--- */
uint8_t ledsAnimationFrame() {
	return _led_animation_frames;
}


/* ---
void ledsDisable() - enable / disable the entire LED subsystem
--- */
void ledsDisable() {
	_led_enabled = false;
	_charlieplex_all_off();
}

/* ---
void ledsEnable() - enable / disable the entire LED subsystem
--- */
void ledsEnable() {
	_led_enabled = true;
}
#endif // LEDS_ENABLED
