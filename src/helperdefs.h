
// MIN/MAX/ABS macros
#define MIN(a,b)	((a<b)?(a):(b))
#define MAX(a,b)	((a>b)?(a):(b))
#define ABS(x)		((x>0)?(x):(-x))
