/* ************************************************************************************
* File:    touch.h
* Date:    2020.05.04
* Author:  Bradan Lane Studio
*
* This content may be redistributed and/or modified as outlined under the MIT License
*
* ************************************************************************************/

/* ---

## touch.h - digital touch sensor measurements

The touch sensing is based on measuring a capacitance change between a conductive
sensor created on the PCB and earth (self capacitance sensor).

The code measures the time for a pin to go from LOW to HIGH. When no capacitance is present,
the time is short. The time increases with the increase in capacitance.

The human body will act as a capacitor. So it is possible to detect with the sensor
is touched because the measure time increases.

Using a threshold, the capacitance touch sensor may be used as a momentary contact button.

A small sensor area will work with no isolation layer - aka without solder mask. However, there is the
risk of static discharge into the pin.

The sensor pin is momentarily pulled HIGH by the code.
Then allowed to dissipate through a large resistor (1M Ohm or larger).
The number of cycles of a tight loop is measured until the sensor pin is LOW again.
The more capacitance present, the longer it takes for the pin to be LOW.

The threshold value(s) are determined by the combination of the chosen resistor size,
the size of the touch pad on the PCB, and the presence or absence of solder mask.
It is possible that multiple sensors on a PCB will each have different thresholds.
The settings in `config.h` provides for this possibility.

**Note:** It is highly advisable to read all of the comments within the module to understand
the function of the circuit and its relation to the code.

**Wiring:**
 - connect 1M resistor between sensor pin and GND
 - connect sensor pin to touch pad on PCB

--------------------------------------------------------------------------
--- */

#if !TOUCH_ENABLED

#define touchInit()
#define touchIsInited() (false)
#define touchClear()
#define touchTouching() (0)
#define touchTouched() (0)
#define touchValue(num) (0)
#define touchCalibrate() (0)

#else

/*
#### Background

This Code is adapted from the DigitalTouch library by
(Rainer Urlacher)[https://github.com/RainerU/DigitalTouch]

Which was adapted from the AnalogTouch library by
(NicoHood)[https://github.com/NicoHood/AnalogTouch]


Copyright (c) 2019 Bradan Lane Studio

Copyright (c) 2018 Rainer Urlacher

published under the MIT License, see github repositories for details

It is advisable to read Rainer Urlacher's original comments.
What follows are notes and details for a specific implementation.

*/


// WARNING: these values are affected by F_CPU and F_SCALER
// threshold of 2 and limit of 4 worked with an interrupt rate of 10KHz but not a 2KHz
// review leds.h setup to determine interupt rate, then change these values as needed

// these are bit fields
#define TS_TOUCH_OFF 		0x00
#define TS_TOUCH_ON 		0x01
#define TS_TOUCH_UNREAD 	0x02

#define TS_MIN_TIME	50	// milliseconds

#define TOUCH_DELTA (TOUCH_MEASUREMENT_ITERATIONS+2)

uint8_t _touch_inited;

// ts_pins, ts_ddr, and ts_port arrays are define in the config file
uint8_t _ts_states[TOUCH_COUNT];
uint16_t _ts_values[TOUCH_COUNT];
uint32_t _ts_next_run;

uint16_t _ts_calibration[TOUCH_COUNT];

// by using inline code, the following functions will resolve to single line macros when all of the sensors are within a single PORT

static inline void _SET_AS_INPUT(uint8_t id, uint8_t pin) {
#if !TOUCH_PORTS_2X
		TOUCH_PORT_0.DIRCLR = pin;
		TOUCH_PORT_0.OUTCLR = pin;
		 // no pullup
		switch(pin) {
			case 0: {TOUCH_PORT_0.PIN0CTRL &= ~(PORT_PULLUPEN_bm);} break;
			case 1: {TOUCH_PORT_0.PIN1CTRL &= ~(PORT_PULLUPEN_bm);} break;
			case 2: {TOUCH_PORT_0.PIN2CTRL &= ~(PORT_PULLUPEN_bm);} break;
			case 3: {TOUCH_PORT_0.PIN3CTRL &= ~(PORT_PULLUPEN_bm);} break;
			case 4: {TOUCH_PORT_0.PIN4CTRL &= ~(PORT_PULLUPEN_bm);} break;
			case 5: {TOUCH_PORT_0.PIN5CTRL &= ~(PORT_PULLUPEN_bm);} break;
			case 6: {TOUCH_PORT_0.PIN6CTRL &= ~(PORT_PULLUPEN_bm);} break;
			case 7: {TOUCH_PORT_0.PIN7CTRL &= ~(PORT_PULLUPEN_bm);} break;
		}
#else
	if (id < TOUCH_SPLIT) {
		TOUCH_PORT_0.DIRCLR = pin;
		TOUCH_PORT_0.OUTCLR = pin;
		switch(pin) {
			case 0: {TOUCH_PORT_0.PIN0CTRL &= ~(PORT_PULLUPEN_bm);} break;
			case 1: {TOUCH_PORT_0.PIN1CTRL &= ~(PORT_PULLUPEN_bm);} break;
			case 2: {TOUCH_PORT_0.PIN2CTRL &= ~(PORT_PULLUPEN_bm);} break;
			case 3: {TOUCH_PORT_0.PIN3CTRL &= ~(PORT_PULLUPEN_bm);} break;
			case 4: {TOUCH_PORT_0.PIN4CTRL &= ~(PORT_PULLUPEN_bm);} break;
			case 5: {TOUCH_PORT_0.PIN5CTRL &= ~(PORT_PULLUPEN_bm);} break;
			case 6: {TOUCH_PORT_0.PIN6CTRL &= ~(PORT_PULLUPEN_bm);} break;
			case 7: {TOUCH_PORT_0.PIN7CTRL &= ~(PORT_PULLUPEN_bm);} break;
		}
	} else {
		TOUCH_PORT_1.DIRCLR = pin;
		TOUCH_PORT_1.OUTCLR = pin;
		switch(pin) {
			case 0: {TOUCH_PORT_1.PIN0CTRL &= ~(PORT_PULLUPEN_bm);} break;
			case 1: {TOUCH_PORT_1.PIN1CTRL &= ~(PORT_PULLUPEN_bm);} break;
			case 2: {TOUCH_PORT_1.PIN2CTRL &= ~(PORT_PULLUPEN_bm);} break;
			case 3: {TOUCH_PORT_1.PIN3CTRL &= ~(PORT_PULLUPEN_bm);} break;
			case 4: {TOUCH_PORT_1.PIN4CTRL &= ~(PORT_PULLUPEN_bm);} break;
			case 5: {TOUCH_PORT_1.PIN5CTRL &= ~(PORT_PULLUPEN_bm);} break;
			case 6: {TOUCH_PORT_1.PIN6CTRL &= ~(PORT_PULLUPEN_bm);} break;
			case 7: {TOUCH_PORT_1.PIN7CTRL &= ~(PORT_PULLUPEN_bm);} break;
		}
	}
#endif
}

static inline void _SET_AS_OUTPUT(uint8_t id, uint8_t pin) {
#if !TOUCH_PORTS_2X
		TOUCH_PORT_0.DIRSET = pin;
#else
		if (id < TOUCH_SPLIT) {
			TOUCH_PORT_0.DIRSET = pin;
		} else {
			TOUCH_PORT_1.DIRSET = pin;
		}
#endif
}

static inline void _SET_LOW(uint8_t id, uint8_t pin) {
#if !TOUCH_PORTS_2X
		TOUCH_PORT_0.OUTCLR = pin;
#else
		if (id < TOUCH_SPLIT) {
			TOUCH_PORT_0.OUTCLR = pin;
		} else {
			TOUCH_PORT_1.OUTCLR = pin;
		}
#endif
}

static inline void _SET_HIGH(uint8_t id, uint8_t pin) {
#if !TOUCH_PORTS_2X
		TOUCH_PORT_0.OUTSET = pin;
#else
		if (id < TOUCH_SPLIT) {
			TOUCH_PORT_0.OUTSET = pin;
		} else {
			TOUCH_PORT_1.OUTSET = pin;
		}
#endif
}

static inline uint8_t _READ(uint8_t id, uint8_t pin) {
#if !TOUCH_PORTS_2X
	return (TOUCH_PORT_0.IN & pin);
#else
	if (id < TOUCH_SPLIT)
		return (TOUCH_PORT_0.IN & pin);
	return (TOUCH_PORT_1.IN & pin);
#endif
}


void _touch_update() {
	uint8_t ts_id;
	uint8_t ts_pin;
	uint8_t ts_iterations;
	uint16_t ts_measure;
	uint16_t ts_value;

	// avoid running too often
	if (clockMillis() < _ts_next_run)
		return;
	_ts_next_run = clockMillis() + TS_MIN_TIME;

	// initially set all pins as input
	for (ts_id=0; ts_id < TOUCH_COUNT; ts_id++) {
		ts_pin = _touch_pins[ts_id];
		//_SET_AS_INPUT(ts_id, ts_pin);
		_SET_AS_OUTPUT(ts_id, ts_pin);
		_SET_LOW(ts_id, ts_pin);
	}
	_delay_ms(5);

	for (ts_id=0; ts_id < TOUCH_COUNT; ts_id++) {
		ts_value = 0;
		ts_pin = _touch_pins[ts_id];
		ts_iterations = TOUCH_MEASUREMENT_ITERATIONS;

		for (ts_iterations = 0; ts_iterations < TOUCH_MEASUREMENT_ITERATIONS; ts_iterations++) {
			ts_measure = 0;

			_SET_AS_OUTPUT(ts_id, ts_pin);
			_SET_HIGH(ts_id, ts_pin); 				// charge the sensor pin
			_delay_ms(1);
			_SET_AS_INPUT(ts_id, ts_pin);			// immediately switch from output to input

			while ((_READ(ts_id, ts_pin))) {
				ts_measure++;						// count the number of loops it takes for the sensor to go LOW
				if (ts_measure > TOUCH_UPPER_LIMIT)	// a count over the sensor limit is definitely a touch event
					break;
			}
			_SET_AS_OUTPUT(ts_id, ts_pin);
			_SET_LOW(ts_id, ts_pin);

			ts_value += ts_measure;
		}

		_ts_values[ts_id] = ts_value;

		if (ts_value < (_ts_calibration[ts_id] + TOUCH_DELTA)) {
			_ts_states[ts_id] = TS_TOUCH_OFF;
		}
		else {
			uint8_t old_state = _ts_states[ts_id];
			// if we were ON and are still ON, then we preserve the old 'unread' state
			uint8_t flags = (old_state & TS_TOUCH_ON) ? (old_state & TS_TOUCH_UNREAD) : TS_TOUCH_UNREAD;
			_ts_states[ts_id] = TS_TOUCH_ON | flags;
		}
	}
}

/* ---
void touchClear() - clear all data associated with the touch sensors
--- */
void touchClear() {
	uint8_t i;
	for (i = 0; i < TOUCH_COUNT; i++) {
		_ts_states[i] = 0;
		_ts_values[i] = 0;
	}
	_ts_next_run = 0;
}

/* ---
uint16_t touchTouching() - compute the values for all touch sensors and return their state as a bit field (1 bit per sensor)
--- */
uint16_t touchTouching() {
	if (!_touch_inited) return 0;

	_touch_update();	// insure the sensors are up to date

	uint16_t t = 0;
	for (uint8_t i = 0; i < TOUCH_COUNT; i++) {
		if ((_ts_states[i] & TS_TOUCH_ON))
			t |= (1 << i);
	}
	return t;
}

/* ---
uint16_t touchTouched() - compute the values for all touch sensors and return their state as a bit field (1 bit per sensor) then clear the sensors
--- */
uint16_t touchTouched() {
	uint16_t t = 0;
	t = touchTouching();
	touchClear();
	return t;
}

/* ---
uint8_t touchValue(uint8_t num) - fetch the current numerical measurement for a single sensor (this is only valid after touchTouching())
--- */
// peek at the most recent computed value; mostly just for debugging
uint8_t touchValue(uint8_t num) {
	if (!_touch_inited) return 0;
	return (_ts_values[num]);
}

/* ---
void touchInit() - called to initialize the touch sensor state structures
--- */
void touchInit() {
	touchClear();
	_ts_next_run = 0;	// we track 'last ran' time to avoid processing too quickly
	// set touch sensor power pin as output HIGH
#ifdef TOUCH_PWR_PIN
	TOUCH_PWR_PORT.DIRSET = TOUCH_PWR_PIN;
	TOUCH_PWR_PORT.OUTSET = TOUCH_PWR_PIN;
#endif

	for (uint8_t i = 0; i < TOUCH_COUNT; i++) {
		uint8_t val;
#if defined(EEPROM_TOUCH_CALIBRATION)
		val = eepromReadByte(EEPROM_TOUCH_CALIBRATION + i);
#else
		val = 0;
#endif
		if ((val > 0) && (val < TOUCH_UPPER_LIMIT))
			_ts_calibration[i] = val;
		else
			_ts_calibration[i] = TOUCH_DEFAULT_THRESHOLD;
	}
	_touch_inited = true;
}

/* ---
bool touchInited() - returns true is the touch module has been initialized
--- */
bool touchIsInited() {
	return _touch_inited;
}

/* ---
bool touchCalibrate() - calibrate the touch sensors and save results to eeprom
--- */
bool touchCalibrate() {
	if (!_touch_inited) return false;

	bool pass = true;
	for (uint8_t t = 0; t < TOUCH_COUNT; t++) {
		_ts_calibration[t] = 0;
	}
#define TS_CALIBRATION_SAMPLES 4
	// first get ; we will average them below
	for (uint8_t i = 0; i < TS_CALIBRATION_SAMPLES; i++) {
		touchTouching();

		for (uint8_t t = 0; t < TOUCH_COUNT; t++) {
			uint8_t val = touchValue(t);
			if (val > TOUCH_UPPER_LIMIT) {
				// if we have a value which is out of bounds, then we assume something is wrong;
				pass = false;
				val = TOUCH_DEFAULT_THRESHOLD;
			}
			_ts_calibration[t] += val;
		}
	}

	printPrintf("TSET: ");
	for (uint8_t t = 0; t < TOUCH_COUNT; t++) {
		// average the samples values for each sensor; round up
		_ts_calibration[t] = (_ts_calibration[t] + (TS_CALIBRATION_SAMPLES - 1)) / TS_CALIBRATION_SAMPLES;
		_ts_calibration[t] += (TS_CALIBRATION_SAMPLES - 1);	// we give ourselves some upper latitude to avoid false positives
#if defined(EEPROM_TOUCH_CALIBRATION)
		eepromWriteByte(EEPROM_TOUCH_CALIBRATION + t, _ts_calibration[t]);
#endif
		uartPutByte(' ');
		printPrintf(" %02d", _ts_calibration[t]);
	}
	uartPutByte('\n');
	touchClear();

	return pass;
}

/* ---
uint8_t touchValue(uint8_t num) - fetch the current numerical measurement for a single sensor (this is only valid after touchTouching())
--- */
// peek at the most recent computed value; mostly just for debugging
uint8_t touchCalibrationValue(uint8_t num) {
	return (_ts_calibration[num]);
}

#endif /* TOUCH_ENABLED */
