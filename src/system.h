/* ************************************************************************************
* File:    system.h
* Date:    2022.12.04
* Author:  Bradan Lane Studio
*
* This content may be redistributed and/or modified as outlined under the MIT License
*
* ************************************************************************************/

/* ---

## system.h - system setup

--------------------------------------------------------------------------
--- */



// this must be included first
#include <avr/power.h>

// current implementation uses the internal reference at 2Mhz to make 9600 baud serial more stable
#define F_CPU 2000000UL // define it now as 2 MHz unsigned long

#include <avr/interrupt.h>
#include <avr/io.h>
#include <avr/pgmspace.h> // NOTE: with the ATMega4809, PROGMEM is a NOP
#include <avr/sleep.h>
//#include <avr/boot.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <util/atomic.h>
#include <util/delay.h>


/*
 * Access to flash-mapped program memory!
 * 104 is a 128k part like AVR128DA
 * 102 is a 64k part like AVR64DB, or AVR64DD
 * 103 is a 48k or less part like the AVR32DA, or the AVR16DD - or the tinyAVR 0/1/2-series, or megaAVR 0-series.
 * the important difference for 103 parts is that they have the entire flash
 * mapped to the data address space, while 102 and 104 only have a 32k chunk of it mapped.
 * 102's have 2 sections, 104's have 4 sections.
 */
#if (__AVR_ARCH__ == 104)
  #define PROGMEM_MAPPED   __attribute__(( __section__(".FLMAP_SECTION3")))
  #define PROGMEM_SECTION0 __attribute__(( __section__(".FLMAP_SECTION0")))
  #define PROGMEM_SECTION1 __attribute__(( __section__(".FLMAP_SECTION1")))
  #define PROGMEM_SECTION2 __attribute__(( __section__(".FLMAP_SECTION2")))
  #define PROGMEM_SECTION3 __attribute__(( __section__(".FLMAP_SECTION3")))
#elif (__AVR_ARCH__ == 102)
  #define PROGMEM_MAPPED   __attribute__(( __section__(".FLMAP_SECTION1")))
  #define PROGMEM_SECTION0 __attribute__(( __section__(".FLMAP_SECTION0")))
  #define PROGMEM_SECTION1 __attribute__(( __section__(".FLMAP_SECTION1")))
#else
  // __AVR_ARCH__ == 103, so all of the flash is memory mapped, and the linker
  // will automatically leave const variables in flash.
  #define PROGMEM_MAPPED
#endif



static void systemInit() {
#if defined(CHIP_ATMEGA4809)
	// FUSE 2 is OSCCFG and 0x00=20Mhz 0x01= 16Mhz
	// set internal oscillator to 16Mhz and then DIV8 to get 2Mhz
	// set internal oscillator to default (20Mhz) and then DIV10 to get 2Mhz

	_PROTECTED_WRITE(CLKCTRL.MCLKCTRLB, (CLKCTRL_PDIV_10X_gc | CLKCTRL_PEN_bm));
	_PROTECTED_WRITE(CLKCTRL.MCLKCTRLA, (!CLKCTRL_CLKOUT_bm)); // set to 16MHz requires fuse change: PortaProg = echo 'set 2 01'
															   //_PROTECTED_WRITE(CLKCTRL.MCLKCTRLA, (!CLKCTRL_CLKOUT_bm | CLKCTRL_CLKSEL_OSC20M_gc));	// set to 20MHz
															   //_delay_ms(200);
#elif (defined(CHIP_AVR64DA48) || defined(CHIP_AVR64DD32))

	CPU_CCP = CCP_IOREG_gc;	// unlock protected register
	CLKCTRL.MCLKCTRLB = CLKCTRL_PDIV_8X_gc /* 8 */ | 0 << CLKCTRL_PEN_bp /* Prescaler enable: disabled */ ;

	CPU_CCP = CCP_IOREG_gc;	// unlock protected register
	CLKCTRL.OSCHFCTRLA = CLKCTRL_FRQSEL_2M_gc          /* 2 */ ;

#if 0
	// On AVR devices all peripherals are enable from power on reset.
	// This disables all peripherals to save power. (Set all pins to low power mode)
	// Driver shall enable peripheral if used.

	for (uint8_t i = 0; i < 8; i++) {	*((uint8_t *)&PORTA + 0x10 + i) |= 1 << PORT_PULLUPEN_bp;	}
	for (uint8_t i = 0; i < 8; i++) {	*((uint8_t *)&PORTB + 0x10 + i) |= 1 << PORT_PULLUPEN_bp;	}
	for (uint8_t i = 0; i < 8; i++) {	*((uint8_t *)&PORTC + 0x10 + i) |= 1 << PORT_PULLUPEN_bp;	}
	for (uint8_t i = 0; i < 8; i++) {	*((uint8_t *)&PORTD + 0x10 + i) |= 1 << PORT_PULLUPEN_bp;	}
	for (uint8_t i = 0; i < 8; i++) {	*((uint8_t *)&PORTE + 0x10 + i) |= 1 << PORT_PULLUPEN_bp;	}
	for (uint8_t i = 0; i < 8; i++) {	*((uint8_t *)&PORTF + 0x10 + i) |= 1 << PORT_PULLUPEN_bp;	}
#endif

#else
#error NO CHIP DEFINED: either CHIP_ATMEGA4809 or CHIP_AVR64DA48 should be defined =1 in platform.ini
#endif

	_delay_ms(100); // give the system time to warm; eliminates a startup lockup problem
}
