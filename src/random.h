/* ************************************************************************************
* File:    random.h
* Date:    2020.05.04
* Author:  Bradan Lane Studio
*
* This content may be redistributed and/or modified as outlined under the MIT License
*
* ************************************************************************************/

/* ---

## random.h - pseudo random number generated

The random module provides a convenient source of pseudo random numbers. The generator is seeded from the noise of the internal ADC.

**Warning:** The seed is generated from the ADC using the temperature sensor.
If the ADC is high, then the value will always be 1023. Try altering the prescaler.

--------------------------------------------------------------------------
--- */

static uint16_t _seed;

/* ---
void randomInit() - initialize and seed the pseudo random number generator
--- */
void randomInit() {
	// use the ADC to seed random number generation

	ADC0.MUXPOS = ADC_MUXPOS_TEMPSENSE_gc; // use temperature sensor as reference
	ADC0.CTRLC |= ADC_PRESC_DIV4_gc;
	// ADC0.CTRLC |= ADC_REFSEL_INTREF_gc;	// using internal reference voltage
#if defined (CHIP_AVR64DD32)
	ADC0.CTRLB = ADC_SAMPNUM_ACC64_gc;
#endif
	ADC0.CTRLA |= ADC_RESSEL_10BIT_gc;	// 10bit resolution
	ADC0.CTRLA |= ADC_ENABLE_bm;		// enable ADC

	ADC0.COMMAND = ADC_STCONV_bm;		// start conversion
	// wait for conversion to complete
	while (!(ADC0.INTFLAGS & ADC_RESRDY_bm)) {
		;
	}
	_seed = ADC0.RES;					// read result

	ADC0.INTFLAGS = ADC_RESRDY_bm;		// ready for another conversion
	ADC0.CTRLA &= ~(ADC_ENABLE_bm);		// disable ADC
	ADC0.CTRLA = 0;						// disable ADC
	AC0.CTRLA = 0;						// disable the analog comparator

	srand(_seed);
}

/* ---
uint8_t randomGetNum(max) - get an 8 bit  pseudo random number between zero and max
--- */
uint8_t randomGetNum(uint8_t max) {
	return rand() % max;
}

/* ---
uint8_t randomGetByte() - get a pseudo random byte value between zero and 0xFF
--- */
uint8_t randomGetByte() {
	return rand() % 0xFF;
}

/* ---
uint16_t randomGetWord() - get a 16 bit pseudo random number
--- */
uint16_t randomGetWord() {
	return rand();
}

/* ---
uint16_t randomGetSeed() - get the active pseudo random seed value
--- */
uint16_t randomGetSeed() {
	return _seed;
}
