/* ************************************************************************************
* File:    sound.h
* Date:    2020.05.04
* Author:  Bradan Lane Studio
*
* This content may be redistributed and/or modified as outlined under the MIT License
*
* ************************************************************************************/

/* ---

## sound.h - single musical note sound generator

This code is written for the ATMega4809. However, within the comments section you will find
the necessary timer values for the tinyAVR 1-series.

The frequency table is the musical scale starting at C3.

The megaAVR timer is 16bit to support multiple octaves of notes.

The macros `N`, `D`, and `T` are for creating music and sound files with a *packed byte* data format.
--------------------------------------------------------------------------
--- */

#if !SOUND_ENABLED
#define soundInit() {}
#define soundOff() {}
#define soundOn(note) {}
#define soundFreq(freq) {}
#define soundMajorNote(note, octave) {}

#else

// see clock.h for more details about TCBn timers
// clock.h uses TCB0
// sound.h uses TCA0

#define FREQUENCY_ERROR 0.974	// a number GT 1.0 means we need to decrease the frequency

#define FREQ_C4 261.63
#define FREQ_C4S 277.18
#define FREQ_D4 293.66
#define FREQ_D4S 311.13
#define FREQ_E4 329.63
#define FREQ_F4 349.23
#define FREQ_F4S 369.99
#define FREQ_G4 392.00
#define FREQ_G4S 415.30
#define FREQ_A4 440.00
#define FREQ_A4S 466.16
#define FREQ_B4 493.88

#define NOTE_REST	0
#define NOTE_R		0
#define NOTE_C		1
#define NOTE_CS		2
#define NOTE_D		3
#define NOTE_DS		4
#define NOTE_E		5
#define NOTE_F		6
#define NOTE_FS		7
#define NOTE_G		8
#define NOTE_GS		9
#define NOTE_A		10
#define NOTE_AS		11
#define NOTE_B		12

#define NOTE_DOTTED		14
#define NOTE_TRIPLET	15

/* ---
**Note:** We pack the musical notes into 8 bits: ddoonnnn = note duration, octave, note (C thru B)
to extend the capability for music, we use two special 'notes'. These special 'notes' are prefixes
and get the actual note and octave from the data which follows:
 - NOTE_DOTTED means it is 1.5x the duration; uses the next byte
 - NOTE_TRIPLET means the 3 notes fit in the given duration; uses the next 3 bytes

 There are macros to simplify created notes, dotted notes, and triplets: `N(n,o,d)`, `D(n,o,d)`, and `T(n1,o1,n2,o2,n3,o3,d)`.

 Note durations are limited to _WHOLE_, _HALF_, _QUARTER_, and _EIGHT_.
--- */

// macro to pack notes
#define N(n, o, d)						( (((d)&0x03) << 6) | (((o)&0x03) << 4) | ((n)&0x0F) )
#define D(n, o, d)						N(NOTE_DOTTED, o, d), N(n, 0, 0)
#define T(n1, o1, n2, o2, n3, o3, d)	N(NOTE_TRIPLET, o, d), N(n1, o1, 0), N(n2, o2, 0), N(n3, o3, 0)

// macros to unpack notes
#define GET_NOTE(n)				(((n)&0x0F))
#define GET_OCTAVE(n)			(((n) >> 4) & 0x03)
#define GET_DURATION_RAW(n)		(((n) >> 6) & 0x03)

// basic music note durations (no triplets dotted notes, etc.
#define WHOLE	0x03			// 0b11
#define HALF	0x02			// 0b10
#define QUARTER	0x01			// 0b01
#define EIGHTH	0x00			// 0b00


#define SOUND_MAX_NOTES (1+12)
#define SOUND_MAJOR_NOTES (1+7)

#ifdef FREQUENCY_ERROR
#define F2N(n) (( (((F_CPU) * (FREQUENCY_ERROR)) / (n))) -1)	// 16bit values
#else
#define F2N(n) (( ((F_CPU) / (n))) -1)	// 16bit values
#endif

const uint16_t _ecc_scale[SOUND_MAX_NOTES] = {0, F2N(FREQ_C4), F2N(FREQ_C4S), F2N(FREQ_D4), F2N(FREQ_D4S), F2N(FREQ_E4), F2N(FREQ_F4), F2N(FREQ_F4S), F2N(FREQ_G4), F2N(FREQ_G4S), F2N(FREQ_A4), F2N(FREQ_A4S), F2N(FREQ_B4) };
const uint8_t _ecc_major[SOUND_MAJOR_NOTES] = {0, NOTE_C, NOTE_D, NOTE_E, NOTE_F, NOTE_G, NOTE_A, NOTE_B };
#define SOUND_TIMER_VALUE(note)	(_ecc_scale[(note)])

uint8_t _sound_inited;

/* ---
void soundInit() - perform initialization of the sound functions
--- */
void soundInit() {
	_sound_inited = true;

}

/* ---
void soundOff() - turn off sound and disable the timer
--- */
void soundOff() {
	if (!_sound_inited) return;

	cli();
	TCA0.SINGLE.CTRLA &= ~(TCA_SINGLE_ENABLE_bm);		// stop timer
	BUZZER_PORT.DIRCLR = (BUZZER_PIN);	// input
	BUZZER_PORT.OUTCLR = (BUZZER_PIN);	// low
	sei();
}

void _sound_generate(uint16_t interval, bool immediate) {
	if (immediate) {
		if (interval && (TCA0.SINGLE.CTRLA & TCA_SINGLE_ENABLE_bm)) {
			//cli();
//#if (BUZZER_PIN != PIN0_bm) // defined(CHIP_AVR64DD32)
//			if (interval == TCA0.SINGLE.CMP2)
//				return;
//			TCA0.SINGLE.CMP2 = interval;
//#else
			if (interval == TCA0.SINGLE.CMP0)
				return;
			TCA0.SINGLE.CMP0 = interval;
//#endif
			TCA0.SINGLE.CNT = interval / 2;
			//sei();
			return;
		}
	}

	BUZZER_PORT.DIRSET = (BUZZER_PIN); // output

	if (interval) {
		cli();

		// ref material when using TCA0: https://www.avrfreaks.net/forum/tutsoft-avr-01-series-setting-tca0-split-mode-pwm
		//portc pc3 is TCA0 WO3(alt) (v2 code PCB)
		//FIX need to change schematic and PCB to use portc pc0

	    /* set waveform output on PORT C */
		PORTMUX.TCAROUTEA = PORTMUX_TCA0_PORTC_gc;


#if (BUZZER_PIN == PIN2_bm)
	TCA0.SINGLE.CTRLB = TCA_SINGLE_CMP2EN_bm
#elif (BUZZER_PIN == PIN1_bm)
	TCA0.SINGLE.CTRLB = TCA_SINGLE_CMP1EN_bm
#else
	TCA0.SINGLE.CTRLB = TCA_SINGLE_CMP0EN_bm
#endif

	// the generated SQUARE waveform is half HIGH and half LOW
#if 1	// uses Frequency mode
	TCA0.SINGLE.CTRLB |= TCA_SINGLE_WGMODE_FRQ_gc;
	TCA0.SINGLE.CMP0 = interval;
	TCA0.SINGLE.CNT = interval / 2;
#else // uses Period mode
	// WO0, WO1, and WO2, are controlled CMP0, CMP1, and CMP2
	TCA0.SINGLE.CTRLB |= TCA_SINGLE_WGMODE_DSBOTTOM_gc;
	TCA0.SINGLE.PERBUF = interval;
	TCA0.SINGLE.CMP1BUF = interval / 2;

#endif

		/* disable event counting */
#if defined(CHIP_ATMEGA4809)
		TCA0.SINGLE.EVCTRL &= ~(TCA_SINGLE_CNTEI_bm);
//#elif (defined(CHIP_AVR64DA48) || defined(CHIP_AVR64DD32))
//		TCA0.SINGLE.CTRLA = TCA_SINGLE_CLKSEL_DIV2_gc; /* Module Enable: enabled */
//#else
//#error "CONFIGURATION ERROR FOR SOUND, NO CHIP_* DEFINED"
#endif
		TCA0.SINGLE.CTRLA = TCA_SINGLE_ENABLE_bm; /* start timer */

		sei();
	}
}

/* ---
void soundFreq() - turn on sound of frequency by setting up the timer for hardware output on the `BUZZER_PIN`

NOTE: this function does not attempt to stop the current sound when setting a new one; it also does not attempt to start sound
--- */

void soundFreq(uint16_t freq) {
	if (!_sound_inited) return;

	freq = (freq * 2) - 1;
	_sound_generate(F2N(freq), true);
}

/* ---
void soundOn() - turn on sound of musical note by setting up the timer for hardware output on the `BUZZER_PIN`
--- */
void soundOn(uint8_t note, uint8_t octave) {
	if (!_sound_inited) return;

	soundOff();

	if (note && (note > SOUND_MAX_NOTES)) {
		octave += (note / (SOUND_MAX_NOTES - 1));
		note = note % SOUND_MAX_NOTES; // 12 notes plus rest
	}
	uint16_t interval = SOUND_TIMER_VALUE(note);

#if 0
	uartPutString("Note: ");
	uartPutNumberPadded(note, 2, ' ');
	uartPutByte(':');
	uartPutNumber(octave);
	uartPutByte('=');
	uartPutNumberPadded(freq, 4, ' ');
#endif

	// an octave over 6 is probably a coding error
	if (!octave)
		octave = 2;
	if (octave > 6)
		octave = 6;

	for (; octave > 1; octave--)
		interval /= 2; // double the frequency

#if 0
	uartPutByte('=');
	uartPutNumberPadded(freq, 4, ' ');
	uartPutStringNL("");
	_delay_ms(5);
#endif

	_sound_generate(interval, false);
}

void soundMajorNote(uint8_t note, uint8_t octave) {
	if (!_sound_inited) return;

#if 0
	uartPutString("Major: ");
	uartPutNumberPadded(note, 2, ' ');
	uartPutByte(':');
	uartPutNumber(octave);
	uartPutByte('=');
	uartPutStringNL("");
#endif

	if (note && (note >= SOUND_MAJOR_NOTES)) {
		note -= 1;	// ignore the rest
		octave += (note / (SOUND_MAJOR_NOTES - 1));
		note = note % (SOUND_MAJOR_NOTES - 1); // 7 notes plus rest, but skip the rest when wrapping around
		/*
		while (note >= (SOUND_MAJOR_NOTES - 1)) {
			octave++;
			note -= (SOUND_MAJOR_NOTES - 1);
		}
		*/
		note +=1;	// restore the rest
		if (octave > 5)
			octave = 5;	// soundOn will limit the octave to 6 but that is uncomfortably high for musical notes
	}

	soundOn(_ecc_major[note], octave);
}

#endif // end SOUND_ENABLED
