/* ************************************************************************************
* File:    clock.h
* Date:    2020.05.04
* Author:  Bradan Lane Studio
*
* This content may be redistributed and/or modified as outlined under the MIT License
*
* ************************************************************************************/

/* ---

## clock.h - a 1 millisecond reference timer value

The clock module uses TCB0 to establish a 2KHz timer and exposes it with 1 millisecond
accuracy. This is useful for performing timed operations, animations, detecting timeouts, etc.

The internal timer frequency could be changed to accommodate other uses.

In addition to being a simple instrument for timing things, the clock module also
handles the charlieplexed (or multiplexed) LEDs if/when they are enabled and initialized.

**Note 1:** 2Khz was chosen over 10Khz because the amount of time needed to perform the
interrupt task - both incrementing the clock and handling the LEDs.
The time to complete the interrupt affects the ultimate frequency. This may either
be tolerated or the compare/capture value adjusted to compensate.

Using an oscilloscope, a clock compensation value was determined.

It should be noted that this method is not perfect as the 'observer effect' may have effected the measurement
and thus the compensation value. Life sucks sometimes but you gotta do what you gotta do.

**Note 2:** When used in conjunction with LEDs, the led module must be included before the clock module.

--------------------------------------------------------------------------
--- */

// Ref Material: https://www.avrfreaks.net/comment/2416971#comment-2416971



// the follow variable is used within the interrupt to maintain a clock for animation
volatile uint8_t _clock_ticks;	// number of interrupts before we increment out milliseconds
volatile uint32_t _clock_ms;	// animations are done with millisecond accuracy

#define TIMER_FREQ 2000								// 2KHz timer
#define INTERRUPTS_PER_MILLIS (TIMER_FREQ / 1000)	// 1ms reference available to core functions
#define TIMER_INTERVAL ((F_CPU / TIMER_FREQ) - 1)	// computed COMP value
#define TIMER_INTERVAL_COMPENSATION (3)				// compensation for code execution time (+ value to speed up interrupt)

ISR(TCB0_INT_vect) {
	// update our core reference clock
	_clock_ticks++;

	if (_clock_ticks >= INTERRUPTS_PER_MILLIS ) {
		_clock_ticks = 0;
		_clock_ms++;
	}
#if LEDS_ENABLED
	_leds_timer_event();
#endif

	TCB0.INTFLAGS = TCB_CAPT_bm; // must clear the interrupt flag
}

/* ---
void clockInit() - called to initialize the clock; this function should be called very early in the program execution
--- */

void clockInit() {
	_clock_ticks = 0;
	_clock_ms = 0;

	cli();
	TCB0.CCMP = TIMER_INTERVAL - TIMER_INTERVAL_COMPENSATION;	// set the compare value; eg value to get 2Khz interrupt
	TCB0.INTCTRL = TCB_CAPT_bm;									// using capture interrupt; ISR(TCB0_INT_vect)
#if defined(CHIP_ATMEGA4809)
	TCB0.CTRLB = TCB_CCMPEN_bm | TCB_CNTMODE_INT_gc;			// interrupt type
    TCB0.CTRLA |= TCB_ENABLE_bm;								// start timer
#elif (defined(CHIP_AVR64DA48) || defined(CHIP_AVR64DD32))
	TCB0.CTRLA = TCB_CLKSEL_DIV1_gc | TCB_ENABLE_bm;			// start timer
#else
#error NO CHIP DEFINED: either CHIP_ATMEGA4809 or CHIP_AVR64DA48 should be defined =1 in platform.ini
#endif
	sei();
}

/* ---
uint32_t clockMillis() - return the current counter as a 32bit unsigned integer. The counter starts at 0 when clockInit() is first called.
--- */
uint32_t clockMillis() {
	uint32_t t;
	ATOMIC_BLOCK(ATOMIC_RESTORESTATE) {
		t = _clock_ms;
	}
	return t;
}
